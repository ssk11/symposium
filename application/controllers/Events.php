<?php
/**
 * Created by PhpStorm.
 * User: Senthilkumar
 * Date: 9/27/2015
 * Time: 8:11 AM
 */
if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Events extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('tank_auth');
        $this->load->model('events_modal');
    }
    public function index()
    {
        $eveid = $this->security->xss_clean($this->uri->segment(2));
        $eveslug = $this->security->xss_clean($this->uri->segment(3));
        $data['event_details'] = $this->events_modal->retrieve_event_details($eveid, $eveslug);

        $data['title'] = $data['event_details'][0]['eve_title']."-".$data['event_details'][0]['eve_clg'];
        $data['description'] = "Nationalsymposium.in is a free online portal to share and find all types of college events in India.";
        $data['keywords'] = $data['event_details'][0]['eve_title'].",symposium,college events in India,college events,workshops";
        $this->load->view('common/header', $data);
        if (!empty($data['event_details'])) {
            $this->load->view('events/content', $data);
        } else {
            $this->load->view('404/content', $data);
        }
        $this->load->view('common/footer');
        $event_vcount = $this->add_viewcount();
        echo $event_vcount;
    }
    function add_viewcount()
    {
// load cookie helper
        //$this->load->helper('cookie');
// this line will return the cookie which has slug name
        $cid = $this->security->xss_clean($this->uri->segment(2));
        $slug = $this->security->xss_clean($this->uri->segment(3));
        //$check_visitor = $this->input->cookie(urldecode($slug), FALSE);
// this line will return the visitor ip address
        //$ip = $this->input->ip_address();
// if the visitor visit this article for first time then //
        //set new cookie and update article_views column  ..
//you might be notice we used slug for cookie name and ip
//address for value to distinguish between articles  views
//        if ($check_visitor == false) {
//            $cookie = array(
//                "name"   => urldecode($slug),
//                "value"  => $ip,
//                "expire" =>  time() + 7200,
//                "secure" => false
//            );
//            $this->input->set_cookie($cookie);
            $this->events_modal->update_views($cid,urldecode($slug));
        //}
    }
}
