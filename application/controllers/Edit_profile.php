<?php
/**
 * Created by PhpStorm.
 * User: Senthilkumar
 * Date: 9/27/2015
 * Time: 8:11 AM
 */
if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Edit_profile extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('profile_model');
        $this->load->library('tank_auth');
    }
    public function index()
    {
        $userid = $this->tank_auth->get_user_id();
        if ($userid == 0) {
            redirect("login");
        }

        $data['title'] = "Edit Profile - National Symposium";
        $data['description'] = "Nationalsymposium.in is a free online portal to share and find all types of college events in India.";
        $data['keywords'] = "symposium,college events in India,college events,workshops";

        $data['basic_info'] = $this->profile_model->retrieve_basic_info();
        $data['ce_allstates'] = $this->profile_model->retrieve_state();
        $this->load->view('common/header', $data);
        $this->load->view('profile/edit-profile', $data);
        $this->load->view('common/footer');
    }

    public function update_basic_info()
    {
        $userid = $this->tank_auth->get_user_id();
        if ($userid == 0) {
            redirect("login");
        }

        $pro_name = $this->security->xss_clean($this->input->post('pro_name'));
        $pro_mobile = $this->security->xss_clean($this->input->post('pro_mobile'));
        $pro_gender = $this->security->xss_clean($this->input->post('pro_gender'));
        $pro_clg = $this->security->xss_clean($this->input->post('pro_clg'));
        $pro_city = $this->security->xss_clean($this->input->post('pro_city'));
        $pro_state = $this->security->xss_clean($this->input->post('pro_state'));
        $pro_fb = $this->security->xss_clean($this->input->post('pro_fb'));
        $pro_tw = $this->security->xss_clean($this->input->post('pro_tw'));
        $pro_gp = $this->security->xss_clean($this->input->post('pro_gp'));
        $pro_in = $this->security->xss_clean($this->input->post('pro_in'));

        $pro_update = $this->profile_model->edit_basic_profile($pro_name, $pro_mobile, $pro_gender, $pro_clg, $pro_city, $pro_state, $pro_fb, $pro_tw, $pro_gp, $pro_in);

        echo json_encode($pro_update);
        exit;
    }

    public function update_propic()
    {
        $userid = $this->tank_auth->get_user_id();
        if ($userid == 0) {
            redirect("login");
        }
        $data['error'] = "";
        $config['upload_path'] = "./uploads/profile-image";
        $config['allowed_types'] = "jpeg|jpg|JPG|png|gif";
        $config['max_size'] = '1000';
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('ep_pic', false)) {
            $error = array('error' => $this->upload->display_errors());
            $data['error'] = strip_tags($this->upload->display_errors());
            echo json_encode($data['error']);
            exit;
        } else {
            $file_data = $this->upload->data();
            $data['propic'] = base_url() . '/uploads/profile-image/' . $file_data['file_name'];
            $pic_suc = $this->profile_model->update_pro_pic($data['propic']);
            echo json_encode($pic_suc);
            exit;
        }
    }
    public function selcity()
    {
        $stateid = $this->security->xss_clean($this->input->post('stateid'));
        $ce_city = $this->profile_model->retrieve_city($stateid);
        echo json_encode($ce_city);
        exit;

    }
}
