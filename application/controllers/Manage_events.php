<?php
/**
 * Created by PhpStorm.
 * User: Senthilkumar
 * Date: 9/27/2015
 * Time: 8:11 AM
 */
if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Manage_events extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('profile_model');
        $this->load->library('tank_auth');
    }
    public function index()
    {
        $userid = $this->tank_auth->get_user_id();
        if ($userid == 0) {
            redirect("login");
        }
        $data['title'] = "Published Events - National Symposium";
        $data['description'] = "Nationalsymposium.in is a free online portal to share and find all types of college events in India.";
        $data['keywords'] = "symposium,college events in India,college events,workshops";
        $this->load->view('common/header', $data);
        $data['basic_info'] = $this->profile_model->retrieve_basic_info();
        $data['my_pubeve'] = $this->profile_model->retrieve_pubeve();
        $this->load->view('profile/published-events', $data);
        $this->load->view('common/footer');
    }
    public function ureview()
    {
        $userid = $this->tank_auth->get_user_id();
        if ($userid == 0) {
            redirect("login");
        }
        $data['title'] = "Under Review Events - National Symposium";
        $data['description'] = "Nationalsymposium.in is a free online portal to share and find all types of college events in India.";
        $data['keywords'] = "symposium,college events in India,college events,workshops";
        $this->load->view('common/header', $data);
        $data['basic_info'] = $this->profile_model->retrieve_basic_info();
        $data['my_ureveve'] = $this->profile_model->retrieve_ureveve();
        $this->load->view('profile/ureview-events', $data);
        $this->load->view('common/footer');
    }
    public function rejeve()
    {
        $userid = $this->tank_auth->get_user_id();
        if ($userid == 0) {
            redirect("login");
        }
        $data['title'] = "Rejected Events - National Symposium";
        $data['description'] = "Nationalsymposium.in is a free online portal to share and find all types of college events in India.";
        $data['keywords'] = "symposium,college events in India,college events,workshops";
        $this->load->view('common/header', $data);
        $data['basic_info'] = $this->profile_model->retrieve_basic_info();
        $data['my_rjeve'] = $this->profile_model->retrieve_rejeve();
        $this->load->view('profile/rejected-events', $data);
        $this->load->view('common/footer');
    }
}
