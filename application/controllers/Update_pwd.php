<?php
/**
 * Created by PhpStorm.
 * User: Senthilkumar
 * Date: 9/27/2015
 * Time: 8:11 AM
 */
if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Update_pwd extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->helper('security');
        //$this->load->library('security');
        $this->load->library('tank_auth');
        $this->lang->load('tank_auth');
        $this->load->model('profile_model');
    }
    public function index()
    {
        if (!$this->tank_auth->is_logged_in()) {                                // not logged in or not activated
            redirect('login');
        } else {
            $this->form_validation->set_rules('old_password', 'Old Password', 'trim|required|xss_clean');
            $this->form_validation->set_rules('new_password', 'New Password', 'trim|required|xss_clean|min_length['.$this->config->item('password_min_length', 'tank_auth').']|max_length['.$this->config->item('password_max_length', 'tank_auth').']|alpha_dash');
            $this->form_validation->set_rules('confirm_new_password', 'Confirm new Password', 'trim|required|xss_clean|matches[new_password]');

            $data['errors'] = array();

            if ($this->form_validation->run()) {                                // validation ok
                if ($this->tank_auth->change_password(
                    $this->form_validation->set_value('old_password'),
                    $this->form_validation->set_value('new_password'))) {    // success
                    $this->session->set_flashdata('message', 'Changed Successfully');
                } else {                                                        // fail
                    $errors = $this->tank_auth->get_error_message();
                    foreach ($errors as $k => $v) {
                        $data['errors'][$k] = $this->lang->line($v);
                    }
                }
            }
            $data['title'] = "Change password - National Symposium";
            $data['description'] = "Nationalsymposium.in is a free online portal to share and find all types of college events in India.";
            $data['keywords'] = "symposium,college events in India,college events,workshops";

            $data['basic_info'] = $this->profile_model->retrieve_basic_info();
            $this->load->view('common/header',$data);
            $this->load->view('profile/update-password',$data);
            $this->load->view('common/footer');
        }
    }
}
