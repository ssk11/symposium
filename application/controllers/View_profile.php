<?php
/**
 * Created by PhpStorm.
 * User: Senthilkumar
 * Date: 9/27/2015
 * Time: 8:11 AM
 */
if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class View_profile extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('profile_model');
        $this->load->library('tank_auth');
    }
    public function index()
    {
        $userid = $this->tank_auth->get_user_id();
        if ($userid == 0) {
            redirect("login");
        }
        $data['profile_details'] = $this->profile_model->retrieve_profile();

        $data['title'] = "My profile - National Symposium";
        $data['description'] = "Nationalsymposium.in is a free online portal to share and find all types of college events in India.";
        $data['keywords'] = "symposium,college events in India,college events,workshops";

        $this->load->view('common/header', $data);
        $this->load->view('profile/view-profile', $data);
        $this->load->view('common/footer');
    }
}
