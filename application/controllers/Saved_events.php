<?php
/**
 * Created by PhpStorm.
 * User: Senthilkumar
 * Date: 9/27/2015
 * Time: 8:11 AM
 */
if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Saved_events extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('profile_model');
        $this->load->library('tank_auth');
    }
    public function index()
    {
        $userid = $this->tank_auth->get_user_id();
        if ($userid == 0) {
            redirect("login");
        }
        $this->load->view('common/header');
        $data['my_upsavedeve'] = $this->profile_model->retrieve_upsavedeve();
        $data['my_hissavedeve'] = $this->profile_model->retrieve_hissavedeve();
        $this->load->view('profile/saved-events', $data);
        $this->load->view('common/footer');
    }
    public function del_eve()
    {
        $userid = $this->tank_auth->get_user_id();
        if ($userid == 0) {
            redirect("login");
        }

        $e_v_id = $this->security->xss_clean($this->input->post('e_v_id'));

        $del_eve = $this->profile_model->del_eve($e_v_id);

        echo json_encode($del_eve);
        exit;
    }
}
