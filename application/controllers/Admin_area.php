<?php
/**
 * Created by PhpStorm.
 * User: Senthilkumar
 * Date: 9/27/2015
 * Time: 8:11 AM
 */
if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Admin_area extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('tank_auth');
        $this->load->model('admin_model');
    }
    public function index()
    {
        $userid = $this->tank_auth->get_user_id();
        if ($userid == 0) {
            redirect("login");
        }
        if ($userid == 1) {
            $data['title'] = "National Symposium | All college events in India";
            $data['description'] = "Nationalsymposium.in is a free online portal to share and find all types of college events in India.";
            $data['keywords'] = "symposium,college events in India,college events,workshops";
            $data['total_records'] = $this->admin_model->get_total();
            $data['latest_event'] = $this->admin_model->retrieve_latest_event();
            $this->load->view('common/header', $data);
            $this->load->view('adminarea/content', $data);
            $this->load->view('common/footer');
        } else {
            redirect("login");
        }
    }
    public function edetail()
    {
        $userid = $this->tank_auth->get_user_id();
        if ($userid == 0) {
            redirect("login");
        }
        if ($userid == 1) {
            $data['title'] = "National Symposium | All college events in India";
            $data['description'] = "Nationalsymposium.in is a free online portal to share and find all types of college events in India.";
            $data['keywords'] = "symposium,college events in India,college events,workshops";
        $this->load->view('common/header', $data);
        $eveid = $this->security->xss_clean($this->uri->segment(2));
        $eveslug = $this->security->xss_clean($this->uri->segment(3));
        $data['event_details'] = $this->admin_model->retrieve_event_details($eveid, $eveslug);
        if (!empty($data['event_details'])) {
            $this->load->view('adminarea/edetail', $data);
        } else {
            $this->load->view('404/content', $data);
        }
        $this->load->view('common/footer');
        } else {
            redirect("login");
        }
    }

    public function eappo()
    {
        $userid = $this->tank_auth->get_user_id();
        if ($userid != 1) {
            redirect("login");
        }
        $eveid = $this->security->xss_clean($this->input->post('eid'));
        $eveslug = $this->security->xss_clean($this->input->post('eslug'));
        $eapprove = $this->admin_model->eapprove($eveid, $eveslug);
        echo json_encode($eapprove);
        exit;
    }

    public function rejo()
    {
        $userid = $this->tank_auth->get_user_id();
        if ($userid != 1) {
            redirect("login");
        }
        $eveid = $this->security->xss_clean($this->input->post('eid'));
        $eveslug = $this->security->xss_clean($this->input->post('eslug'));
        $erejo = $this->admin_model->rejo($eveid, $eveslug);
        echo json_encode($erejo);
        exit;
    }

    public function users()
    {
        $userid = $this->tank_auth->get_user_id();
        if ($userid == 0) {
            redirect("login");
        }
        if ($userid == 1) {
            $data['title'] = "National Symposium | All college events in India";
            $data['description'] = "Nationalsymposium.in is a free online portal to share and find all types of college events in India.";
            $data['keywords'] = "symposium,college events in India,college events,workshops";
            $data['Usercount'] = $this->admin_model->get_ucount();
            $data['all_users'] = $this->admin_model->retrieve_users();
            $this->load->view('common/header', $data);
            $this->load->view('adminarea/users', $data);
            $this->load->view('common/footer');
        } else {
            redirect("login");
        }
    }

}
