<?php
/**
 * Created by PhpStorm.
 * User: Senthilkumar
 * Date: 9/27/2015
 * Time: 8:11 AM
 */
if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Static_p extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('tank_auth');
        $this->load->model('contact_model');
        $this->load->library('form_validation');
    }
    public function index()
    {
        $data['title'] = "About Us - National Symposium";
        $data['description'] = "Nationalsymposium.in is a free online portal to share and find all types of college events in India.";
        $data['keywords'] = "symposium,college events in India,college events,workshops";
        $this->load->view('common/header', $data);
        $this->load->view('static/about');
        $this->load->view('common/footer');
    }
    public function contact()
    {
        $data['title'] = "Contact Us - National Symposium";
        $data['description'] = "Nationalsymposium.in is a free online portal to share and find all types of college events in India.";
        $data['keywords'] = "symposium,college events in India,college events,workshops";
        $this->load->view('common/header', $data);
        $this->load->view('static/contact');
        $this->load->view('common/footer');
    }
    public function sendmail()
    {
        $this->form_validation->set_rules('cfullname', 'Fullname', 'required|trim');
        $this->form_validation->set_rules('cemail', 'Email', 'required|trim');
        $this->form_validation->set_rules('cregarding', 'Regarding', 'required|trim');
        $this->form_validation->set_rules('cmsg', 'Message', 'required|trim');
        if ($this->form_validation->run() === false) {
            $errors = validation_errors();
            echo json_encode($errors);
            exit;
        } else {
            $cfullname = $this->security->xss_clean($this->input->post('cfullname'));
            $cemail = $this->security->xss_clean($this->input->post('cemail'));
            $cregarding = $this->security->xss_clean($this->input->post('cregarding'));
            $cmsg = $this->security->xss_clean($this->input->post('cmsg'));

            $ce_done = $this->contact_model->Emailtoadmin($cfullname,$cemail,$cregarding,$cmsg);
            echo json_encode($ce_done);
            exit;
        }
    }
    public function privacy()
    {
        $data['title'] = "Privacy Policy - National Symposium";
        $data['description'] = "Nationalsymposium.in is a free online portal to share and find all types of college events in India.";
        $data['keywords'] = "symposium,college events in India,college events,workshops";
        $this->load->view('common/header', $data);
        $this->load->view('static/privacy');
        $this->load->view('common/footer');
    }
}
