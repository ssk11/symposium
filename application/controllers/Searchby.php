<?php
/**
 * Created by PhpStorm.
 * User: Senthilkumar
 * Date: 9/27/2015
 * Time: 8:11 AM
 */
if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Searchby extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        // load Pagination library
        $this->load->library('pagination');
        $this->load->helper('url');
        $this->load->model('searchby_model');
        $this->load->library('tank_auth');
    }
    public function index()
    {
        $data['type_name'] = $this->searchby_model->retrieve_type();

        $data['title'] = "College events by type - National Symposium";
        $data['description'] = "Nationalsymposium.in is a free online portal to share and find all types of college events in India.";
        $data['keywords'] = "symposium,college events in India,college events,workshops";

        $this->load->view('common/header',$data);
        $this->load->view('search/type',$data);
        $this->load->view('common/footer');
    }
    public function bytype()
    {
        $data = "";
        $evetid = $this->security->xss_clean($this->uri->segment(2));
        $evetname = $this->security->xss_clean($this->uri->segment(3));
        $config = array();
        $limit_per_page = 10;
        $start_index = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $total_records = $this->searchby_model->get_total($evetid);
        if ($total_records > 0)
        {
            // get current page records
            $data['latest_event'] = $this->searchby_model->retrieve_ltype_event($limit_per_page, $start_index, $evetid);
            $config['base_url'] = base_url("event-types/" . $evetid ."/". $evetname . "/page/");
            $config['total_rows'] = $total_records;
            $config['per_page'] = $limit_per_page;
            $config["uri_segment"] = 4;

            $config['full_tag_open'] = '<ul class="pagination justify-content-center mb-4">';
            $config['full_tag_close'] = '</ul>';

            $config['first_link'] = 'First';
            $config['first_tag_open'] = '<li class="page-item disabled">';
            $config['first_tag_close'] = '</li>';

            $config['last_link'] = 'Last';
            $config['last_tag_open'] = '<li class="page-item">';
            $config['last_tag_close'] = '</li>';

            $config['cur_tag_open'] = '<li class="active"><a class="page-link" href="">';
            $config['cur_tag_close'] = '</a></li>';

            $config['num_tag_open'] = '<li class="page">';
            $config['num_tag_close'] = '</li>';

            $config['anchor_class'] = 'page-link';

            $this->pagination->initialize($config);

            $data["links"] = $this->pagination->create_links();
        }
        if(!empty($data['latest_event'])) {
            $data['title'] = "Upcoming " . $data['latest_event'][0]['type_name'] . " Events - National Symposium";
            $data['description'] = "Nationalsymposium.in is a free online portal to share and find all types of college events in India.";
            $data['keywords'] = $data['latest_event'][0]['type_name'] . ",symposium,college events in India,college events,workshops";
        } else {
            $data['title'] =  "No events found - National Symposium";
            $data['description'] = "Nationalsymposium.in is a free online portal to share and find all types of college events in India.";
            $data['keywords'] = "symposium,college events in India,college events,workshops";
        }

        $this->load->view('common/header',$data);
        $this->load->view('search/bytype', $data,$evetid);
        $this->load->view('common/footer');
    }

    public function city()
    {
        $data['title'] = "College events by city - National Symposium";
        $data['description'] = "Nationalsymposium.in is a free online portal to share and find all types of college events in India.";
        $data['keywords'] = "symposium,college events in India,college events,workshops";
        $data['city_name'] = $this->searchby_model->retrieve_city_event();
        $this->load->view('common/header',$data);
        $this->load->view('search/city',$data);
        $this->load->view('common/footer');
    }

    public function bycity()
    {
        $data = "";
        $evecid = $this->security->xss_clean($this->uri->segment(2));
        $config = array();
        $limit_per_page = 10;
        $start_index = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $total_records = $this->searchby_model->get_city_total($evecid);
        if ($total_records > 0) {
            // get current page records
            $data['latest_event'] = $this->searchby_model->retrieve_lcity_event($limit_per_page, $start_index, $evecid);

            $config['base_url'] = base_url("city/" . $evecid . "/page/");
            $config['total_rows'] = $total_records;
            $config['per_page'] = $limit_per_page;
            $config["uri_segment"] = 4;

            $config['full_tag_open'] = '<ul class="pagination justify-content-center mb-4">';
            $config['full_tag_close'] = '</ul>';

            $config['first_link'] = 'First';
            $config['first_tag_open'] = '<li class="page-item disabled">';
            $config['first_tag_close'] = '</li>';

            $config['last_link'] = 'Last';
            $config['last_tag_open'] = '<li class="page-item">';
            $config['last_tag_close'] = '</li>';

            $config['cur_tag_open'] = '<li class="active"><a class="page-link" href="">';
            $config['cur_tag_close'] = '</a></li>';

            $config['num_tag_open'] = '<li class="page">';
            $config['num_tag_close'] = '</li>';

            $config['anchor_class'] = 'page-link';

            $this->pagination->initialize($config);

            $data["links"] = $this->pagination->create_links();
        }
        if(!empty($data['latest_event'])) {
            $data['title'] = "Upcoming college events in ".$data['latest_event'][0]['eve_city']." - National Symposium";
            $data['description'] = "Nationalsymposium.in is a free online portal to share and find all types of college events in India.";
            $data['keywords'] = " college events in ".$data['latest_event'][0]['eve_city'].",symposium,college events in India,college events,workshops";
        } else {
            $data['title'] =  "No events found - National Symposium";
            $data['description'] = "Nationalsymposium.in is a free online portal to share and find all types of college events in India.";
            $data['keywords'] = "symposium,college events in India,college events,workshops";
        }

        $this->load->view('common/header',$data);
        $this->load->view('search/bycity', $data, $evecid);
        $this->load->view('common/footer');
    }

    public function state()
    {
        $data['title'] = "College events by state - National Symposium";
        $data['description'] = "Nationalsymposium.in is a free online portal to share and find all types of college events in India.";
        $data['keywords'] = "symposium,college events in India,college events,workshops";

        $data['state_name'] = $this->searchby_model->retrieve_state();

        $this->load->view('common/header',$data);
        $this->load->view('search/state',$data);
        $this->load->view('common/footer');
    }
    public function bystate()
    {
        $data = "";
        $evesid = $this->security->xss_clean($this->uri->segment(2));
        $evesname = $this->security->xss_clean($this->uri->segment(3));
        $config = array();
        $limit_per_page = 10;
        $start_index = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
        $total_records = $this->searchby_model->get_state_total($evesid);

        if ($total_records > 0) {
            // get current page records
            $data['latest_event'] = $this->searchby_model->retrieve_lstate_event($limit_per_page, $start_index, $evesid);

            $config['base_url'] = base_url("state/" . $evesid ."/". $evesname . "/page/");
            $config['total_rows'] = $total_records;
            $config['per_page'] = $limit_per_page;
            $config["uri_segment"] = 5;

            $config['full_tag_open'] = '<ul class="pagination justify-content-center mb-4">';
            $config['full_tag_close'] = '</ul>';

            $config['first_link'] = 'First';
            $config['first_tag_open'] = '<li class="page-item disabled">';
            $config['first_tag_close'] = '</li>';

            $config['last_link'] = 'Last';
            $config['last_tag_open'] = '<li class="page-item">';
            $config['last_tag_close'] = '</li>';

            $config['cur_tag_open'] = '<li class="active"><a class="page-link" href="">';
            $config['cur_tag_close'] = '</a></li>';

            $config['num_tag_open'] = '<li class="page">';
            $config['num_tag_close'] = '</li>';

            $config['anchor_class'] = 'page-link';

            $this->pagination->initialize($config);

            $data["links"] = $this->pagination->create_links();
        }
        if(!empty($data['latest_event'])) {
            $data['title'] = "Upcoming college events in ".$data['latest_event'][0]['state_name']." - National Symposium";
            $data['description'] = "Nationalsymposium.in is a free online portal to share and find all types of college events in India.";
            $data['keywords'] = "College events in ".$data['latest_event'][0]['state_name'].",symposium,college events in India,college events,workshops";
        } else {
            $data['title'] =  "No events found - National Symposium";
            $data['description'] = "Nationalsymposium.in is a free online portal to share and find all types of college events in India.";
            $data['keywords'] = "symposium,college events in India,college events,workshops";
        }

        $this->load->view('common/header',$data);
        $this->load->view('search/bystate', $data, $evesid);
        $this->load->view('common/footer');
    }

    public function department()
    {
        $data['title'] = "College events by departments - National Symposium";
        $data['description'] = "Nationalsymposium.in is a free online portal to share and find all types of college events in India.";
        $data['keywords'] = "symposium,college events in India,college events,workshops";

        $data['department_name'] = $this->searchby_model->retrieve_department();

        $this->load->view('common/header',$data);
        $this->load->view('search/department',$data);
        $this->load->view('common/footer');
    }
    public function bydepartment()
    {
        $data = "";
        $evedid = $this->security->xss_clean($this->uri->segment(2));
        $evedname = $this->security->xss_clean($this->uri->segment(3));
        $config = array();
        $limit_per_page = 10;
        $start_index = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
        $total_records = $this->searchby_model->get_department_total($evedid);

        if ($total_records > 0) {
            // get current page records
            $data['latest_event'] = $this->searchby_model->retrieve_ldepartment_event($limit_per_page, $start_index, $evedid);

            $config['base_url'] = base_url("event-departments/" . $evedid ."/". $evedname . "/page/");
            $config['total_rows'] = $total_records;
            $config['per_page'] = $limit_per_page;
            $config["uri_segment"] = 5;

            $config['full_tag_open'] = '<ul class="pagination justify-content-center mb-4">';
            $config['full_tag_close'] = '</ul>';

            $config['first_link'] = 'First';
            $config['first_tag_open'] = '<li class="page-item disabled">';
            $config['first_tag_close'] = '</li>';

            $config['last_link'] = 'Last';
            $config['last_tag_open'] = '<li class="page-item">';
            $config['last_tag_close'] = '</li>';

            $config['cur_tag_open'] = '<li class="active"><a class="page-link" href="">';
            $config['cur_tag_close'] = '</a></li>';

            $config['num_tag_open'] = '<li class="page">';
            $config['num_tag_close'] = '</li>';

            $config['anchor_class'] = 'page-link';

            $this->pagination->initialize($config);

            $data["links"] = $this->pagination->create_links();
        }
        if(!empty($data['latest_event'])) {
            $data['title'] = "Upcoming college events in ".$data['latest_event'][0]['dept_name']." - National Symposium";
            $data['description'] = "Nationalsymposium.in is a free online portal to share and find all types of college events in India.";
            $data['keywords'] = "College events in ".$data['latest_event'][0]['dept_name'].",symposium,college events in India,college events,workshops";
        } else {
            $data['title'] =  "No events found - National Symposium";
            $data['description'] = "Nationalsymposium.in is a free online portal to share and find all types of college events in India.";
            $data['keywords'] = "symposium,college events in India,college events,workshops";
        }

        $this->load->view('common/header',$data);
        $this->load->view('search/bydepartment', $data, $evedid);
        $this->load->view('common/footer');
    }

}
