<?php
/**
 * Created by PhpStorm.
 * User: Senthilkumar
 * Date: 9/27/2015
 * Time: 8:11 AM
 */
if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Home extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        // load Pagination library
        $this->load->library('pagination');
        $this->load->helper('url');
        $this->load->model('home_modal');
        $this->load->library('tank_auth');
    }
    public function index()
    {
        $data['title'] = "National Symposium - Search and share all college events in India";
        $data['description'] = "Nationalsymposium.in is a free online portal to share and find all types of college events in India.";
        $data['keywords'] = "symposium,college events in India,college events,workshops";
        $data['latest_event'] = $this->home_modal->retrieve_home_event();

        $this->load->view('common/header', $data);
        $this->load->view('home/main', $data);
        $this->load->view('common/footer');
    }
    public function latest_events()
    {
        $data = "";
        $config = array();
        $limit_per_page = 10;
        $start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $total_records = $this->home_modal->get_total();

        $data['title'] = "National Symposium - Search and share all college events in India";
        $data['description'] = "Nationalsymposium.in is a free online portal to share and find all types of college events in India.";
        $data['keywords'] = "symposium,college events in India,college events,workshops";

        if ($total_records > 0)
        {
            // get current page records
            $data['latest_event'] = $this->home_modal->retrieve_latest_event($limit_per_page, $start_index);

            $config['base_url'] = base_url("latest-events/page/");
            $config['total_rows'] = $total_records;
            $config['per_page'] = $limit_per_page;
            $config["uri_segment"] = 3;

            $config['full_tag_open'] = '<ul class="pagination justify-content-center mb-4">';
            $config['full_tag_close'] = '</ul>';

            $config['first_link'] = 'First';
            $config['first_tag_open'] = '<li class="page-item disabled">';
            $config['first_tag_close'] = '</li>';

            $config['last_link'] = 'Last';
            $config['last_tag_open'] = '<li class="page-item">';
            $config['last_tag_close'] = '</li>';

            $config['cur_tag_open'] = '<li class="active"><a class="page-link" href="">';
            $config['cur_tag_close'] = '</a></li>';

            $config['num_tag_open'] = '<li class="page">';
            $config['num_tag_close'] = '</li>';

            $config['anchor_class'] = 'page-link';

            $this->pagination->initialize($config);

            $data["links"] = $this->pagination->create_links();
        }

        $this->load->view('common/header', $data);
        //$data['upcoming_event'] = $this->home_modal->retrieve_upcoming_event();
        $this->load->view('home/content', $data);
        $this->load->view('common/footer');
    }
}
