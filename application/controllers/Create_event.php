<?php
/**
 * Created by PhpStorm.
 * User: Senthilkumar
 * Date: 9/27/2015
 * Time: 8:11 AM
 */
if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Create_event extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('create_event_modal');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('tank_auth');
    }
    public function index()
    {
        $userid = $this->tank_auth->get_user_id();
        if ($userid == 0) {
            redirect("login");
        }
        $data['title'] = "Create Event - National Symposium";
        $data['description'] = "Create event and publish your college events through Nationalsymposium.in";
        $data['keywords'] = "symposium,college events in India,college events,workshops";

        $data['ce_allstates'] = $this->create_event_modal->retrieve_state();
        $data['ce_type'] = $this->create_event_modal->retrieve_type();
        $data['ce_department'] = $this->create_event_modal->retrieve_department();
        $this->load->view('common/header', $data);
        $this->load->view('create-event/content',$data);
        $this->load->view('common/footer');
    }
    public function create()
    {
        $userid = $this->tank_auth->get_user_id();
        if ($userid == 0) {
            redirect("login");
        }
        $this->form_validation->set_rules('ce_title', 'Event Name', 'htmlspecialchars|trim|required|min_length[6]|max_length[200]');
        $this->form_validation->set_rules('ce_sdate', 'Start Date', 'required|trim');
        $this->form_validation->set_rules('ce_edate', 'End Date', 'required|trim');
        $this->form_validation->set_rules('ce_ltdate', 'Last Date for Registration', 'required|trim');
        $this->form_validation->set_rules('ce_rfee', 'Registration Fees', 'trim');
        $this->form_validation->set_rules('ce_decp', 'Description', 'htmlspecialchars|required|min_length[100]|trim');
        $this->form_validation->set_rules('ce_evelist', 'Event List', 'htmlspecialchars|required|min_length[100]|trim');
        $this->form_validation->set_rules('ce_evetype', 'Event Type', 'required|trim');
        $this->form_validation->set_rules('ce_clg', 'College', 'required|trim');
        $this->form_validation->set_rules('ce_adrs', 'Address', 'required|trim');
        $this->form_validation->set_rules('ce_city', 'City', 'required|trim');
        $this->form_validation->set_rules('ce_state', 'State', 'required|trim');
        $this->form_validation->set_rules('ce_pin', 'Zipcode', 'required|trim');
        $this->form_validation->set_rules('ce_evedept', 'Event Department', 'required|trim');
        $this->form_validation->set_rules('ce_banner', 'Banner', 'trim');
        $this->form_validation->set_rules('ce_evweb', 'Event Website', 'trim');
        $this->form_validation->set_rules('ce_reglink', 'Registration Link', 'trim');
        $this->form_validation->set_rules('ce_clglink', 'College Website', 'trim');
        $this->form_validation->set_rules('ce_fb', 'Facebook URL', 'trim');
        $this->form_validation->set_rules('ce_tw', 'Twitter URL', 'trim');
        $this->form_validation->set_rules('ce_yt', 'Youtube URL', 'trim');
        $this->form_validation->set_rules('ce_ctperson', 'Contact Person Name', 'required|trim');
        $this->form_validation->set_rules('ce_cphone', 'Contact phone', 'required|trim');
        $this->form_validation->set_rules('ce_cmail', 'Contact Email', 'required|trim');

        if ($this->form_validation->run() === false) {
            $errors = validation_errors();
            echo json_encode($errors);
            exit;
        } else {
            $ce_done = $this->create_event_modal->create_event();
            $clgc = $this->create_event_modal->create_clg();
            $cityc = $this->create_event_modal->create_city();
            $ce_ar = array('Successfully Created', $ce_done,$clgc,$cityc);
            echo json_encode($ce_ar);
            exit;
        }
    }

    public function upload_cover_pic()
    {
        $userid = $this->tank_auth->get_user_id();
        if ($userid == 0) {
            redirect("login");
        }
        $data['error'] = "";
        $config['upload_path'] = "./uploads/cover-image";
        $config['allowed_types'] = "jpeg|jpg|JPG|png|gif";
        $config['max_size'] = '1000';
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('ce_banner', false)) {
            $error = array('error' => $this->upload->display_errors());
            $data['error'] = strip_tags($this->upload->display_errors());
            echo json_encode($data['error']);
            exit;
        } else {
            $file_data = $this->upload->data();
            $cov_suc = base_url() . '/uploads/cover-image/' . $file_data['file_name'];
            echo json_encode($cov_suc);
            exit;
        }
    }

    public function eve_success()
    {
        $userid = $this->tank_auth->get_user_id();
        if ($userid == 0) {
            redirect("login");
        }
        $eveid = $this->security->xss_clean($this->uri->segment(1));
        $ce_done = $this->create_event_modal->retrieve_event_title($eveid);
        if(!empty($ce_done)){
            $data['title'] = "Created Successfully - National Symposium";
            $data['description'] = "Create event and publish your college events through Nationalsymposium.in";
            $data['keywords'] = "symposium,college events in India,college events,workshops";
            $this->load->view('common/header', $data);
            $this->load->view('create-event/success');
            $this->load->view('common/footer');
        } else {
            redirect("home");
        }
    }

    public function selcity()
    {
        $stateid = $this->security->xss_clean($this->input->post('stateid'));
        $keyword = $this->security->xss_clean($this->input->post('query'));
        $ce_city = $this->create_event_modal->retrieve_city($stateid,$keyword);
        echo json_encode($ce_city);
        exit;

    }

    public function getclgname(){
        $keyword=$this->security->xss_clean($this->input->post('query'));
        $data=$this->create_event_modal->GetRow($keyword);
        echo json_encode($data);
    }

}
