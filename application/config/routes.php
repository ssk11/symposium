<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['latest-events'] = 'home/latest_events';
$route['latest-events/page'] = 'home/latest_events';
$route['latest-events/page/(:num)'] = 'home/latest_events';
$route['login'] = "/auth/login";
$route['logout'] = "/auth/logout";
$route['register'] = "/auth/register";
$route['forgot-password'] = "/auth/forgot_password";
$route['change-password'] = "/auth/change_password";
$route['activate/:num/:any'] = "/auth/activate/$1/$2";
$route['reset_password/:num/:any'] = "/auth/reset_password/$1/$2";
$route['edit-profile'] = 'edit_profile';
$route['profile'] = 'view_profile';
$route['update-password'] = 'update_pwd';
$route['published-events'] = 'manage_events';
$route['under-review-events'] = 'manage_events/ureview';
$route['rejected-events'] = 'manage_events/rejeve';
$route['create-event'] = 'create_event';
$route['(:num)/successfully-created'] = 'create_event/eve_success';
$route['e/(:num)/(:any)'] = 'events';
$route['event-types'] = 'searchby';
$route['event-types/(:num)/(:any)'] = 'searchby/bytype';
$route['event-types/(:num)/(:any)/page'] = 'searchby/bytype';
$route['event-types/(:num)/(:any)/page/(:num)'] = 'searchby/bytype';
$route['college-events-by-city'] = 'searchby/city';
$route['city/(:num)/(:any)'] = 'searchby/bycity';
$route['city/(:num)/page'] = 'searchby/bycity';
$route['city/(:num)/page/(:num)'] = 'searchby/bycity';
$route['college-events-by-state'] = 'searchby/state';
$route['state/(:num)/(:any)'] = 'searchby/bystate';
$route['state/(:num)/college-events-in-(:any)/page'] = 'searchby/bystate';
$route['state/(:num)/college-events-in-(:any)/page/(:num)'] = 'searchby/bystate';
$route['event-departments'] = 'searchby/department';
$route['event-departments/(:num)/(:any)'] = 'searchby/bydepartment';
$route['event-departments/(:num)/(:any)/page'] = 'searchby/bydepartment';
$route['event-departments/(:num)/(:any)/page/(:num)'] = 'searchby/bydepartment';

$route['ns-admin'] = 'admin_area';
$route['ns-users'] = 'admin_area/users';
$route['nse/(:num)/(:any)'] = 'admin_area/edetail';

$route['about-us'] = 'static_p';
$route['contact'] = 'static_p/contact';
$route['privacy-policy'] = 'static_p/privacy';