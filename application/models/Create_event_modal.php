<?php
/**
 * Created by PhpStorm.
 * User: Senthilkumar
 * Date: 9/26/2015
 * Time: 4:32 PM
 */
class Create_event_modal extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
        $this->load->library('email');
        $this->load->library('tank_auth');
    }

    public function create_event()
    {
        $slug = url_title($this->input->post('ce_title'), 'dash', TRUE);
        $postby = $this->tank_auth->get_user_id();
        $postuser = $this->tank_auth->get_username();

        $data = array(
            'eve_title' => $this->security->xss_clean($this->input->post('ce_title')),
            'eve_slug' => $this->security->xss_clean($slug),
            'eve_description' => $this->security->xss_clean($this->input->post('ce_decp')),
            'eve_evelist' => $this->security->xss_clean($this->input->post('ce_evelist')),
            'eve_evetype' => $this->security->xss_clean($this->input->post('ce_evetype')),
            'eve_clg' => $this->security->xss_clean($this->input->post('ce_clg')),
            'eve_adrs' => $this->security->xss_clean($this->input->post('ce_adrs')),
            'eve_city' => $this->security->xss_clean($this->input->post('ce_city')),
            'eve_state' => $this->security->xss_clean($this->input->post('ce_state')),
            'eve_pin' => $this->security->xss_clean($this->input->post('ce_pin')),
            'eve_dept' => $this->security->xss_clean($this->input->post('ce_evedept')),
            'eve_sdate' => $this->security->xss_clean($this->input->post('ce_sdate')),
            'eve_edate' => $this->security->xss_clean($this->input->post('ce_edate')),
            'eve_ltdate' => $this->security->xss_clean($this->input->post('ce_ltdate')),
            'eve_rfee' => $this->security->xss_clean($this->input->post('ce_rfee')),
            'eve_banner' => $this->security->xss_clean($this->input->post('ce_banner')),
            'eve_evweb' => $this->security->xss_clean($this->input->post('ce_evweb')),
            'eve_reglink' => $this->security->xss_clean($this->input->post('ce_reglink')),
            'eve_clglink' => $this->security->xss_clean($this->input->post('ce_clglink')),
            'eve_fb' => $this->security->xss_clean($this->input->post('ce_fb')),
            'eve_tw' => $this->security->xss_clean($this->input->post('ce_tw')),
            'eve_yt' => $this->security->xss_clean($this->input->post('ce_yt')),
            'eve_ctperson' => $this->security->xss_clean($this->input->post('ce_ctperson')),
            'eve_cphone' => $this->security->xss_clean($this->input->post('ce_cphone')),
            'eve_cmail' => $this->security->xss_clean($this->input->post('ce_cmail')),
            'eve_status' => '0',
            'eve_pid' => $postby,
            'eve_puser' => $postuser
        );
        //print_r($data);exit;
        $this->db->set('eve_postdate', 'NOW()', FALSE);
        $this->db->insert('ev_events', $data);
        $insert_id = $this->db->insert_id();

        if ($this->db->affected_rows() > 0)
        {
            return $insert_id;
        }
        else
        {
            return "some problem";
        }
        //return $this->db->affected_rows();
        //echo $this->db->last_query();
        //exit;
    }

    public function create_clg() {
        $clg_name = $this->security->xss_clean($this->input->post('ce_clg'));
        $data = array(
            'clg_name' => $this->security->xss_clean($this->input->post('ce_clg')),
        );
        $this->db->where('clg_name',$clg_name);
        $q = $this->db->get('ns_college');

        if ( $q->num_rows() > 0 )
        {
            return "college";
        } else {

            $this->db->insert('ns_college', $data);
            $insert_id = $this->db->insert_id();

            if ($this->db->affected_rows() > 0)
            {
                return $insert_id;
            }
            else
            {
                return "college some problem";
            }
        }
        //$this->db->insert('ns_college', $data);
    }

    public function create_city() {
        $city_name = $this->security->xss_clean($this->input->post('ce_city'));
        $state_id = $this->security->xss_clean($this->input->post('ce_state'));
        $data = array(
            'city_name' => $this->security->xss_clean($this->input->post('ce_city')),
            'state_id' => $this->security->xss_clean($this->input->post('ce_state')),
        );

        $this->db->where('city_name',$city_name);
        $this->db->where('state_id',$state_id);
        $q = $this->db->get('ns_cities');

        if ( $q->num_rows() > 0 )
        {
            return "city";
        } else {
            $this->db->insert('ns_cities', $data);
            $insert_id = $this->db->insert_id();

            if ($this->db->affected_rows() > 0)
            {
                return $insert_id;
            }
            else
            {
                return "city some problem";
            }
        }

        //$this->db->insert('ns_cities', $data);
    }

    public function retrieve_event_title($eveid)
    {
        $this -> db -> select('eve_title');
        $this -> db -> from('ev_events');
        $this -> db -> where('eve_id', $eveid);

        $query = $this -> db -> get();
        return $query->result_array();
    }
    public function retrieve_state()
    {
        $this -> db -> select('*');
        $this -> db -> from('ns_states');

        $query = $this -> db -> get();
        return $query->result_array();
    }
    public function retrieve_city($stateid,$keyword)
    {
        $this -> db -> select('*');
        $this -> db -> from('ns_cities');
        $this -> db -> where('state_id', $stateid);
        $this->db->like("city_name", $keyword);

        $query = $this -> db -> get();
        return $query->result_array();
    }
    public function retrieve_type()
    {
        $this -> db -> select('*');
        $this -> db -> from('ns_type');

        $query = $this -> db -> get();
        return $query->result_array();
        //echo $this->db->last_query();
    }
    public function retrieve_department()
    {
        $this -> db -> select('*');
        $this -> db -> from('ns_departments');

        $query = $this -> db -> get();
        return $query->result_array();
        //echo $this->db->last_query();
    }
    public function GetRow($keyword) {
        $this -> db -> select('clg_name');
        $this -> db -> from('ns_college');
        $this->db->like("clg_name", $keyword);
        $query = $this -> db -> get();
        return $query->result_array();
    }
}