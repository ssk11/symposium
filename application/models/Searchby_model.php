<?php
/**
 * Created by PhpStorm.
 * User: Senthilkumar
 * Date: 9/26/2015
 * Time: 4:32 PM
 */
class Searchby_model extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
        $this->load->helper('url');
    }

    public function retrieve_type()
    {
        $this -> db -> select('*');
        $this -> db -> from('ns_type');

        $query = $this -> db -> get();
        return $query->result_array();
        //echo $this->db->last_query();
        //exit;
    }

    public function retrieve_ltype_event($limit_per_page, $start_index, $evetid)
    {
        $this -> db -> select('ev_events.*,ns_type.*,ns_states.*');
        $this -> db -> from('ev_events');
        $this->db->join('ns_states', 'ev_events.eve_state = ns_states.state_id');
        $this->db->join('ns_type', ''.$evetid.' = ns_type.type_id');
        $this->db->order_by("eve_id", "desc");
        $this->db->where("eve_status", "1");
        $this->db->where("FIND_IN_SET('$evetid',eve_evetype) !=", 0);
        $this->db->limit($limit_per_page, $start_index);

        $query = $this -> db -> get();
        return $query->result_array();
    }
    public function get_total($evetid)
    {
        $this->db->where("FIND_IN_SET('$evetid',eve_evetype) !=", 0);
        $this->db->where("eve_status", "1");
        $this->db->from("ev_events");
        return $this->db->count_all_results();
//        echo $this->db->last_query();
//        exit;
    }
    public function retrieve_city_event()
    {
        $this->db->select('city_id,city_name, count(*)');
        $this->db->from('ns_cities');
        $this->db->join('ev_events', 'ns_cities.city_name = ev_events.eve_city');
        $this->db->where("ev_events.eve_status", "1");
        $this->db->group_by('ns_cities.city_id');

        $query = $this -> db -> get();
        return $query->result_array();
    }
    public function retrieve_lcity_event($limit_per_page, $start_index, $evecid)
    {
        $this -> db -> select('ev_events.*,ns_cities.*,ns_states.*');
        $this -> db -> from('ev_events');
        $this->db->join('ns_cities', 'ev_events.eve_city = ns_cities.city_name');
        $this->db->join('ns_states', 'ev_events.eve_state = ns_states.state_id');
        $this->db->order_by("eve_id", "desc");
        $this->db->where("eve_status", "1");
        $this->db->where('ns_cities.city_id', $evecid);
        $this->db->limit($limit_per_page, $start_index);

        $query = $this -> db -> get();
        return $query->result_array();
        //echo $this->db->last_query();
        //exit;
    }
    public function get_city_total($evecid)
    {
        $this->db->join('ns_cities', 'ev_events.eve_city = ns_cities.city_name');
        $this->db->where("eve_status", "1");
        $this->db->where('ns_cities.city_id', $evecid);
        $this->db->from("ev_events");
        return $this->db->count_all_results();
    }

    public function retrieve_state()
    {
        $this->db->select('state_id,state_name, count(*)');
        $this->db->from('ns_states');
        $this->db->join('ev_events', 'ns_states.state_id = ev_events.eve_state');
        $this->db->where("ev_events.eve_status", "1");
        $this->db->group_by('ns_states.state_id');

        $query = $this -> db -> get();
        return $query->result_array();
        //echo $this->db->last_query();
        //exit;
    }
    public function retrieve_lstate_event($limit_per_page, $start_index, $evesid)
    {
        $this -> db -> select('ev_events.*,ns_states.*');
        $this -> db -> from('ev_events');
        $this->db->join('ns_states', 'ev_events.eve_state = ns_states.state_id');
        $this->db->order_by("eve_id", "desc");
        $this->db->where("eve_status", "1");
        $this->db->where('eve_state', $evesid);
        $this->db->limit($limit_per_page, $start_index);

        $query = $this -> db -> get();
        return $query->result_array();
        //echo $this->db->last_query();
        //exit;
    }
    public function get_state_total($evesid)
    {
        $this->db->where("eve_status", "1");
        $this->db->where('eve_state',$evesid);
        $this->db->from("ev_events");
        return $this->db->count_all_results();
    }
    public function retrieve_department()
    {
        $this -> db -> select('*');
        $this -> db -> from('ns_departments');

        $query = $this -> db -> get();
        return $query->result_array();
        //echo $this->db->last_query();
        //exit;
    }
    public function retrieve_ldepartment_event($limit_per_page, $start_index, $evedid)
    {
        $this -> db -> select('ev_events.*,ns_departments.*,ns_states.*');
        $this -> db -> from('ev_events');
        $this->db->join('ns_states', 'ev_events.eve_state = ns_states.state_id');
        $this->db->join('ns_departments', ''.$evedid.' = ns_departments.dept_id');
        $this->db->order_by("eve_id", "desc");
        $this->db->where("eve_status", "1");
        $this->db->where("FIND_IN_SET('$evedid',eve_dept) !=", 0);
        $this->db->limit($limit_per_page, $start_index);

        $query = $this -> db -> get();
        return $query->result_array();

        //echo $this->db->last_query();
        //exit;
    }
    public function get_department_total($evedid)
    {
        $this->db->where("FIND_IN_SET('$evedid',eve_dept) !=", 0);
        $this->db->where("eve_status", "1");
        $this->db->from("ev_events");
        return $this->db->count_all_results();
    }
}