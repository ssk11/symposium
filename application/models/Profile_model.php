<?php
/**
 * Created by PhpStorm.
 * User: Senthilkumar
 * Date: 9/26/2015
 * Time: 4:32 PM
 */
class Profile_model extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
        $this->load->library('email');
        $this->load->library('tank_auth');
    }

    public function edit_basic_profile($pro_name,$pro_mobile,$pro_gender, $pro_clg, $pro_city, $pro_state,$pro_fb,$pro_tw,$pro_gp,$pro_in) {

        $userid = $this->tank_auth->get_user_id();
        $data = array(
            'fullname' => $this->security->xss_clean($pro_name),
            'mobile' => $this->security->xss_clean($pro_mobile),
            'gender' => $this->security->xss_clean($pro_gender),
            'college' => $this->security->xss_clean($pro_clg),
            'city' => $this->security->xss_clean($pro_city),
            'state' => $this->security->xss_clean($pro_state),
            'fb' => $this->security->xss_clean($pro_fb),
            'tw' => $this->security->xss_clean($pro_tw),
            'in' => $this->security->xss_clean($pro_in),
            'gplus' => $this->security->xss_clean($pro_gp)
        );
        $this->db->set('modified', 'NOW()', FALSE);
        $this -> db -> where('id',$userid);
        $this->db->update('users', $data);
        if ($this->db->affected_rows() > 0)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }

    public function retrieve_basic_info()
    {
        $uid = $this->tank_auth->get_user_id();
        $this -> db -> select('fullname,mobile,gender,college,city,state,propic,fb,tw,in,gplus');
        $this -> db -> from('users');
        $this -> db -> where('id', $uid);

        $query = $this -> db -> get();
        return $query->result_array();
        //echo $this->db->last_query();
    }

    public function retrieve_profile()
    {
        $uid = $this->tank_auth->get_user_id();
        $this -> db -> select('*');
        $this -> db -> from('users');
        //$this->db->join('ns_states', 'users.state = ns_states.state_id');
        //$this->db->join('ns_cities', 'users.city = ns_cities.city_id');
        $this -> db -> where('id', $uid);
        $query = $this -> db -> get();
        return $query->result_array();
        //echo $this->db->last_query();
        //exit;
    }

    public function update_pro_pic($file_name = "")
    {
        $uid = $this->tank_auth->get_user_id();

        $data = array(
            'propic' => $this->security->xss_clean($file_name)
        );
        $this->session->set_userdata($data);
        $this->db->where('id', $uid);
        $this->db->update('users', $data);
        if ($this->db->affected_rows() > 0)
        {
            return 1;
        }
        else
        {
            return 0;
        }
        //echo $this->db->last_query();
    }
    public function retrieve_pubeve()
    {
        $uid = $this->tank_auth->get_user_id();
        $this -> db -> select('*');
        $this -> db -> from('ev_events');
        $this -> db -> where('eve_pid', $uid);
        //$this -> db -> where('eve_sdate >=', date('d/m/Y'));
        $this -> db -> where('eve_status', '1');

        $query = $this -> db -> get();
        return $query->result_array();
        //echo $this->db->last_query();
        //exit;
        //echo $this->db->get_compiled_select();
    }

    public function retrieve_ureveve()
    {
        $uid = $this->tank_auth->get_user_id();
        $this -> db -> select('*');
        $this -> db -> from('ev_events');
        $this -> db -> where('eve_pid', $uid);
        $this -> db -> where('eve_sdate >=', date('Y-m-d'));
        $this -> db -> where('eve_status', '0');

        $query = $this -> db -> get();
        return $query->result_array();
        //echo $this->db->get_compiled_select();
    }

    public function retrieve_rejeve()
    {
        $uid = $this->tank_auth->get_user_id();
        $this -> db -> select('*');
        $this -> db -> from('ev_events');
        $this -> db -> where('eve_pid', $uid);
        $this -> db -> where('eve_status', '2');

        $query = $this -> db -> get();
        return $query->result_array();
        //echo $this->db->get_compiled_select();
    }
    public function retrieve_state()
    {
        $this -> db -> select('*');
        $this -> db -> from('ns_states');

        $query = $this -> db -> get();
        return $query->result_array();
    }
    public function retrieve_city($stateid)
    {
        $this -> db -> select('*');
        $this -> db -> from('ns_cities');
        $this -> db -> where('state_id', $stateid);

        $query = $this -> db -> get();
        return $query->result_array();
        //echo $this->db->last_query();
    }

}