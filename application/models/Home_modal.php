<?php
/**
 * Created by PhpStorm.
 * User: Senthilkumar
 * Date: 9/26/2015
 * Time: 4:32 PM
 */
class Home_modal extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
        $this->load->helper('url');
    }

    public function retrieve_latest_event($limit_per_page, $start_index)
    {
        $this -> db -> select('ev_events.*,ns_states.*');
        $this -> db -> from('ev_events');
        $this->db->join('ns_states', 'ev_events.eve_state = ns_states.state_id');
        $this->db->order_by("eve_id", "desc");
        $this->db->where("eve_status", "1");
        $this->db->limit($limit_per_page, $start_index);

        $query = $this -> db -> get();
        return $query->result_array();
    }
    public function get_total()
    {
        $this->db->where('eve_status','1');
        $this->db->from("ev_events");
        return $this->db->count_all_results();
    }
    public function retrieve_home_event()
    {
        $this -> db -> select('ev_events.eve_id,ev_events.eve_title,ev_events.eve_slug,ev_events.eve_clg,ev_events.eve_city,ev_events.eve_state,ns_states.*');
        $this -> db -> from('ev_events');
        $this->db->join('ns_states', 'ev_events.eve_state = ns_states.state_id');
        $this->db->order_by("eve_id", "desc");
        $this->db->where("eve_status", "1");
        $this->db->limit(4);

        $query = $this -> db -> get();
        return $query->result_array();
        //echo $this->db->last_query();
        //exit;
    }

}