<?php
/**
 * Created by PhpStorm.
 * User: Senthilkumar
 * Date: 9/26/2015
 * Time: 4:32 PM
 */
class Contact_model extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
        $this->load->helper('url');
       $this->load->library('email');
    }

    public function Emailtoadmin($cfullname,$cemail,$cregarding,$cmsg){
        $this->email->set_newline("\r\n");
        $this->email->from('noreply@nationalsymposium.in', "National Symposium");
        $this->email->to("sskumar.muthupet@gmail.com");
        $this->email->subject($cregarding."-Contact from national symposium");
        $this->email->message('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
   <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
   <meta name="format-detection" content="telephone=yes">
   <title>National Symposium</title>
   <style type="text/css">
.ReadMsgBody { width: 100%; background-color: #ffffff; }
.ExternalClass { width: 100%; background-color: #ffffff; }
.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div { line-height: 100%; }
html { width: 100%; }
body { -webkit-text-size-adjust: none; -ms-text-size-adjust: none; }
body { margin: 0; padding: 0; }
table { border-spacing: 0; }
img { display: block !important; }
table td { border-collapse: collapse; }
.yshortcuts a { border-bottom: none !important; }
a { color: #01579B; text-decoration: none; }
 @media only screen and (max-width: 640px) {
body { width: auto !important; }
table[class="table600"] { width: 450px !important; text-align: center !important; }
table[class="table-inner"] { width: 86% !important; }
table[class="table2-2"] { width: 47% !important; }
table[class="table3-3"] { width: 100% !important; text-align: center !important; }
table[class="table1-3"] { width: 29.9% !important; }
table[class="table3-1"] { width: 64% !important; text-align: center !important; }
/* Image */
img[class="img1"] { width: 100% !important; height: auto !important; }
}
 @media only screen and (max-width: 479px) {
body { width: auto !important; }
table[class="table600"] { width: 290px !important; }
table[class="table-inner"] { width: 80% !important; }
table[class="table2-2"] { width: 100% !important; }
table[class="table3-3"] { width: 100% !important; text-align: center !important; }
table[class="table1-3"] { width: 100% !important; }
table[class="table3-1"] { width: 100% !important; text-align: center !important; }
table[class="middle-line"] { display: none !important; }
/* image */
img[class="img1"] { width: 100% !important; }
}
</style>
</head>

<body>
   <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
      <tr>
         <td>
            <!--Header-->

            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#01579B">


               <tr>
                  <td align="center">
                     <table  class="table600"  height="175" bgcolor="#0277BD" width="600" border="0" cellspacing="0" cellpadding="0">

                        <!-- logo -->
                        <tr>
                           <td align="center" height="200" style="font-family: Arial, Helvetica, sans-serif;font-size:14px;font-weight:bold;">
                              <img src="'.base_url().'assets/images/logo.png" width="200" height="auto" alt="logo" />
                           </td>
                        </tr>
                        <!-- end logo --> </table>
                  </td>
               </tr>
               <tr>
                  <td bgcolor="#c1e4fa">
                     <table class="table600" width="600" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#c7e7fb">
                        <tr>
                           <td height="20"></td>
                        </tr>
                     </table>
                  </td>
               </tr>
            </table>

            <!--End Header--> </td>
      </tr>

      <!-- content with 2 buttons -->
      <tr>
         <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
               <tr>
                  <td bgcolor="#f8f8f8">
                     <table class="table600" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
                        <tr>
                           <td height="25"></td>
                        </tr>


                        <!-- content -->
                        <tr>
                           <td align="left" style="font-family: Arial, Helvetica, sans-serif;font-size:14px; color:#000; line-height:24px;padding: 0 20px;">
                              <p>Fullname: '.$cfullname.'</p>

                           </td>
                        </tr>
                        <tr>
                           <td align="left" style="font-family: Arial, Helvetica, sans-serif;font-size:14px; color:#000; line-height:24px;padding: 0 20px;">
                              <p>Email: '.$cemail.'</p>

                           </td>
                        </tr>
                        <tr>
                           <td align="left" style="font-family: Arial, Helvetica, sans-serif;font-size:14px; color:#000; line-height:24px;padding: 0 20px;">
                              <p>Regarding: '.$cregarding.'</p>

                           </td>
                        </tr>
                        <tr>
                           <td align="left" style="font-family: Arial, Helvetica, sans-serif;font-size:14px; color:#000; line-height:24px;padding: 0 20px;">
                              <p>Message: '.$cmsg.'</p>

                           </td>
                        </tr>
                        <!-- end content -->

                        <tr>
                           <td height="15"></td>
                        </tr>
                        <tr>
                           <td>
                              <table width="230" border="0" align="center" cellpadding="0" cellspacing="0">
                                 <tr>
                                    <td>
                                       <!-- button-->
									   <a href="'.site_url().'">
                                       <table style="border-radius:4px" bgcolor="#263238" width="210" border="0" align="center" cellpadding="0" cellspacing="0">
                                          <tr>


                                          </tr>
                                       </table>
									   </a>

                                       <!-- end button --> </td>
                                    <td width="25"></td>
                                    <td>
                                       <!-- button -->



                                       <!-- end button --> </td>
                                 </tr>
                              </table>
                           </td>
                        </tr>
                        <tr>
                           <td height="25"></td>
                        </tr>
                     </table>
                  </td>
               </tr>
               <tr>
                  <td>
                     <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                        <tr>
                           <td align="right">
                              <table width="100%" height="8" border="0" cellpadding="0" cellspacing="0">
                                 <tr>
                                    <td height="4" bgcolor="#f8f8f8" style="border-bottom:1px solid #efeded;"></td>
                                 </tr>
                                 <tr>
                                    <td height="4"></td>
                                 </tr>
                              </table>
                           </td>
                           <td width="65" height="8" bgcolor="#01579B"></td>
                           <td>
                              <table width="100%" height="8" border="0" cellpadding="0" cellspacing="0">
                                 <tr>
                                    <td height="4" bgcolor="#f8f8f8" style="border-bottom:1px solid #efeded;"></td>
                                 </tr>
                                 <tr>
                                    <td height="4"></td>
                                 </tr>
                              </table>
                           </td>
                        </tr>
                     </table>
                  </td>
               </tr>
            </table>
         </td>
      </tr>
      <!-- end content with 2 buttons -->

      <!--footer info-->
      <tr>
         <td>
            <table width="100%   " border="0" align="center" cellpadding="0" cellspacing="0">

               <tr>
                  <td height="30" align="center">
                     <table width="600" border="0" align="center" cellpadding="0" cellspacing="0" class="table600">
                        <tr>
                           <td>
                              <table class="table-inner" width="280" border="0" align="center" cellpadding="0" cellspacing="0">
                                 <tr>
                                    <td width="78" valign="middle" style="border-collapse: collapse;">
                                       <table width="100%" height="1" border="0" align="right" cellpadding="0" cellspacing="0" style="border-collapse: collapse;">
                                          <tr>
                                             <td height="1" width="100%" bgcolor="#eae9e9" style="border-collapse: collapse;"></td>
                                          </tr>
                                       </table>
                                    </td>
                                    <td width="65" height="8"align="center" bgcolor="#01579B" style="max-width:65px;"></td>
                                    <td width="78" valign="middle" style="border-collapse: collapse;">
                                       <table border="0" cellpadding="0" cellspacing="0" width="100%" height="1" style="border-collapse: collapse;">
                                          <tr>
                                             <td height="1" width="100%" bgcolor="#eae9e9" style="border-collapse: collapse;"></td>
                                          </tr>
                                       </table>
                                    </td>
                                 </tr>
                              </table>
                           </td>
                        </tr>
                     </table>
                  </td>
               </tr>
               <tr>
                  <td height="15"></td>
               </tr>
               <tr>
                  <td align="center" bgcolor="#e2e2e2">
                     <table class="table600" width="600" border="0" cellspacing="0" cellpadding="0" bgcolor="#e8e8e8">
                        <tr>
                           <td height="20"></td>
                        </tr>
                        <tr>
                           <td align="center" valign="top">
                              <table class="table-inner" width="600" border="0" cellspacing="0" cellpadding="0">

                                 <!-- notification -->

                                 <!-- end notification -->
                                 <tr>
                                    <td height="20"></td>
                                 </tr>

                                </table>
                           </td>
                        </tr>
                        <tr>
                           <td height="20"></td>
                        </tr>
                     </table>
                  </td>
               </tr>
               <tr>
                  <td align="center" bgcolor="#d9d9d9">
                     <table class="table600" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
                        <tr>
                           <td height="30" align="center" bgcolor="#e1e1e1" style="font-family: Helvetica, Arial, sans-serif; font-size:11px; color:#6b6b6b;">
                              Copyright �
                              <a href="'.site_url().'" style="color:#01579B; text-decoration:none;">www.nationalsymposium.in</a>
                              , All rights reserved
                           </td>
                        </tr>
                     </table>
                  </td>
               </tr>
            </table>

            <!--End footer info--> </td>
      </tr>
   </table>
</body>
</html>');
        //$this->email->send();
        if($this->email->send())
        {
            return 1;
        }
            else
        {
           return 0;
           //echo $this->email->print_debugger();
        }

    }
}