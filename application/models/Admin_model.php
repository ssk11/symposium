<?php
/**
 * Created by PhpStorm.
 * User: Senthilkumar
 * Date: 9/26/2015
 * Time: 4:32 PM
 */
class Admin_model extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
        $this->load->helper('url');
    }

    public function retrieve_latest_event()
    {
        $this -> db -> select('ev_events.*,ns_states.*,ns_cities.*');
        $this -> db -> from('ev_events');
        $this->db->join('ns_states', 'ev_events.eve_state = ns_states.state_id');
        $this->db->join('ns_cities', 'ev_events.eve_city = ns_cities.city_name');
        $this->db->order_by("eve_id", "desc");
        $this->db->where("eve_status", "0");

        $query = $this -> db -> get();
        return $query->result_array();
    }

    public function retrieve_event_details($eveid,$eveslug)
    {
        $this -> db -> select('ev_events.*,ns_states.*,ns_cities.*');
        $this -> db -> from('ev_events');
        $this->db->join('ns_states', 'ev_events.eve_state = ns_states.state_id');
        $this->db->join('ns_cities', 'ev_events.eve_city = ns_cities.city_name');

        $this -> db -> where('eve_status', '0');
        $this->db->where("eve_sdate >"."'".date('Y-m-d')."'"."");
        $this -> db -> where('eve_id', $eveid);
        $this -> db -> where('eve_slug', $eveslug);

        $query = $this -> db -> get();

        $r = $query->result_array();

        foreach($r as $key => $value){
            $name = array();
            $q = "SELECT `type_id`,`type_name` FROM `ns_type` where `type_id` IN (".$value['eve_evetype'].")";
            $e = $this->db->query($q);
            $row = $e->result_array();
            foreach($row as $nameKey => $nameValue){
                $name[$nameValue['type_id']] = $nameValue['type_name'];
            }
            $r[$key]['type_name'] = $name;
        }
        foreach($r as $key => $value){
            $name = array();
            $q = "SELECT `dept_id`,`dept_name` FROM `ns_departments` where `dept_id` IN (".$value['eve_dept'].")";
            $e = $this->db->query($q);
            $row = $e->result_array();
            foreach($row as $nameKey => $nameValue){
                $name[$nameValue['dept_id']] = $nameValue['dept_name'];
            }
            $r[$key]['dept_name'] = $name;
        }
        //echo $this->db->last_query();
        //echo "<pre>".print_r($r,true)."</pre>";
        return  $r;

        //exit;
    }

    public function get_total()
    {
        $this->db->where('eve_status','0');
        $this->db->from("ev_events");
        return $this->db->count_all_results();
    }

    public function eapprove($eveid,$eveslug) {

        $this -> db -> where('eve_id',$eveid);
        $this -> db -> where('eve_slug',$eveslug);
        $this->db->set('eve_status','1',false);
        $this->db->update('ev_events');
        if ($this->db->affected_rows() > 0)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }

    public function rejo($eveid,$eveslug) {

        $this -> db -> where('eve_id',$eveid);
        $this -> db -> where('eve_slug',$eveslug);
        $this->db->set('eve_status','2',false);
        $this->db->update('ev_events');
        if ($this->db->affected_rows() > 0)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }

    public function retrieve_users()
    {
        $this -> db -> select('*');
        $this -> db -> from('users');
        $this->db->order_by("id", "desc");

        $query = $this -> db -> get();
        return $query->result_array();
    }
    public function get_ucount()
    {
        $this->db->from("users");
        return $this->db->count_all_results();
    }

}