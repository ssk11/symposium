<?php
/**
 * Created by PhpStorm.
 * User: Senthilkumar
 * Date: 9/26/2015
 * Time: 4:32 PM
 */
class Events_modal extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
        $this->load->helper('url');
    }

    public function retrieve_event_details($eveid,$eveslug)
    {
        $this -> db -> select('ev_events.*,ns_states.*');
        $this -> db -> from('ev_events');
        $this->db->join('ns_states', 'ev_events.eve_state = ns_states.state_id');

        $this -> db -> where('eve_status', '1');
        $this -> db -> where('eve_id', $eveid);
        $this -> db -> where('eve_slug', $eveslug);

        $query = $this -> db -> get();

        $r = $query->result_array();

        foreach($r as $key => $value){
            $name = array();
            $q = "SELECT `type_id`,`type_name` FROM `ns_type` where `type_id` IN (".$value['eve_evetype'].")";
            $e = $this->db->query($q);
            $row = $e->result_array();
            foreach($row as $nameKey => $nameValue){
                $name[$nameValue['type_id']] = $nameValue['type_name'];
            }
            $r[$key]['type_name'] = $name;
        }
        foreach($r as $key => $value){
            $name = array();
            $q = "SELECT `dept_id`,`dept_name` FROM `ns_departments` where `dept_id` IN (".$value['eve_dept'].")";
            $e = $this->db->query($q);
            $row = $e->result_array();
            foreach($row as $nameKey => $nameValue){
                $name[$nameValue['dept_id']] = $nameValue['dept_name'];
            }
            $r[$key]['dept_name'] = $name;
        }
//        echo $this->db->last_query();
        //echo "<pre>".print_r($r,true)."</pre>";
        return  $r;

//        exit;
    }

    function update_views($cid,$slug) {
// return current article views
        $this->db->where('eve_slug', urldecode($slug));
        $this->db->where('eve_id', $cid);
        $this->db->select('eve_views');
        $count = $this->db->get('ev_events')->row();
// then increase by one
        $this->db->where('eve_slug', urldecode($slug));
        $this->db->where('eve_id', $cid);
        $this->db->set('eve_views', ($count->eve_views + 1));
        $this->db->update('ev_events');
        //echo $this->db->last_query();
        //exit;
    }

    public function retrieve_upcoming_event()
    {
        $this -> db -> select('*');
        $this -> db -> from('ev_events');
        $this->db->where("eve_sdate >"."'".date('Y-m-d')."'"."");
        $this->db->order_by("eve_sdate", "asc");
        $this->db->limit(10);

        $query = $this -> db -> get();
        return $query->result_array();
        //echo $this->db->last_query();
        //exit;
    }


}