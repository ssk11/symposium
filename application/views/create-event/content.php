<script>
    tinymce.init({
        selector: '#ce_decp',
        height : "200",
        mode : "specific_textareas",
        editor_selector : "ce-decp"
    });
    tinymce.init({
        selector: '#ce_evelist',
        height : "200",
        mode : "specific_textareas",
        editor_selector : "ce-evelist"
    });
</script>

<!-- Page Content -->

<section class="consection">
    <div class="container">

        <div class="postfm">
            <div class="row">

                <!-- Post Content Column -->
                <div class="col-md-12">

                    <h1>Please provide all details clearly to get more audience</h1>
                    <h6>If anything missing in this form, please contact admin by <a style="text-decoration: underline;" href="<?php echo base_url('contact');?>" >click here</a></h6>
                    <div class="alert alert-success ce_subpro" style="display: none;" role="alert"></div>
                    <div class="alert alert-danger ce_suberr" style="display: none;" role="alert"></div>
                    <form id="eveFm">
                        <div class="form-group">
                            <label for="ce_title">Event Name<span class="cereq">*</span></label>
                            <input autocomplete="__away" type="text" class="form-control" id="ce_title" placeholder="Enter Event Title...">
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6 input-append date" id="dp3" data-date="" data-date-format="yyyy-mm-dd">
                                <label for="ce_sdate">Start Date<span class="cereq">*</span></label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="fas fa-calendar-alt"></i></div>
                                    </div>
                                    <input autocomplete="__away" type="text" class="form-control" id="ce_sdate" placeholder="yyyy-mm-dd">
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="ce_edate">End Date<span class="cereq">*</span></label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="fas fa-calendar-alt"></i></div>
                                    </div>
                                    <input autocomplete="__away" type="text" class="form-control" id="ce_edate" placeholder="yyyy-mm-dd">
                                </div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6 input-append date" id="dp3" data-date="" data-date-format="dd-mm-yyyy">
                                <label for="ce_ltdate">Last Date for Registration<span class="cereq">*</span></label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="fas fa-calendar-alt"></i></div>
                                    </div>
                                    <input autocomplete="__away" type="text" class="form-control" id="ce_ltdate" placeholder="yyyy-mm-dd">
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="ce_rfee">Registration Fees</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="fas fa-rupee-sign"></i></div>
                                    </div>
                                    <input autocomplete="__away" type="number" class="form-control" id="ce_rfee" placeholder="1000">
                                </div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="ce_decp">Event Description<span class="cereq">*</span></label>
                                <textarea class="form-control ce-decp" id="ce_decp" rows="3"></textarea>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="ce_evelist">Event List<span class="cereq">*</span></label>
                                <textarea class="form-control ce-evelist" id="ce_evelist" rows="3"></textarea>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <label>Event Type<span class="cereq">*</span></label>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-3">
                                <?php foreach (array_slice($ce_type,0,6) as $ce_types): ?>
                                <div class="form-check">
                                    <input name="evetype[]" class="form-check-input evetype" type="checkbox" value="<?php echo $ce_types['type_id']; ?>" id="<?php echo "evtype_".$ce_types['type_id']; ?>">
                                    <label class="form-check-label" for="<?php echo "evtype_".$ce_types['type_id']; ?>">
                                        <?php echo $ce_types['type_name']; ?>
                                    </label>
                                </div>
                                <?php endforeach; ?>
                            </div>
                            <div class="form-group col-md-3">
                                <?php foreach (array_slice($ce_type,6,6) as $ce_types): ?>
                                    <div class="form-check">
                                        <input name="evetype[]" class="form-check-input evetype" type="checkbox" value="<?php echo $ce_types['type_id']; ?>" id="<?php echo "evtype_".$ce_types['type_id']; ?>">
                                        <label class="form-check-label" for="<?php echo "evtype_".$ce_types['type_id']; ?>">
                                            <?php echo $ce_types['type_name']; ?>
                                        </label>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                            <div class="form-group col-md-3">
                                <?php foreach (array_slice($ce_type,12,6) as $ce_types): ?>
                                    <div class="form-check">
                                        <input name="evetype[]" class="form-check-input evetype" type="checkbox" value="<?php echo $ce_types['type_id']; ?>" id="<?php echo "evtype_".$ce_types['type_id']; ?>">
                                        <label class="form-check-label" for="<?php echo "evtype_".$ce_types['type_id']; ?>">
                                            <?php echo $ce_types['type_name']; ?>
                                        </label>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                            <div class="form-group col-md-3">
                                <?php foreach (array_slice($ce_type,18,3) as $ce_types): ?>
                                    <div class="form-check">
                                        <input name="evetype[]" class="form-check-input evetype" type="checkbox" value="<?php echo $ce_types['type_id']; ?>" id="<?php echo "evtype_".$ce_types['type_id']; ?>">
                                        <label class="form-check-label" for="<?php echo "evtype_".$ce_types['type_id']; ?>">
                                            <?php echo $ce_types['type_name']; ?>
                                        </label>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>

                        <hr>
                        <div class="form-group" id="clghol">
                            <label for="ce_clg">College / Business Name<span class="cereq">*</span></label>

                            <input data-provide="typeahead" autocomplete="__away" type="text" class="form-control typeahead" id="ce_clg" placeholder="Enter College / Business Name">
                        </div>
                        <div class="form-group">
                            <label for="ce_adrs">Address<span class="cereq">*</span></label>
                            <input autocomplete="__away" type="text" class="form-control" id="ce_adrs" placeholder="Enter Address">
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="ce_state">State<span class="cereq">*</span></label>
                                <select id="ce_state" class="form-control">
                                    <option selected disabled>Choose State...</option>
                                    <?php foreach ($ce_allstates as $ce_showstates): ?>
                                    <option value="<?php echo $ce_showstates['state_id']; ?>"><?php echo $ce_showstates['state_name']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="ce_city">City / Town<span class="cereq">*</span></label>

                                <input data-provide="typeahead" autocomplete="__away" type="text" class="form-control typeahead" id="ce_city" placeholder="Enter City/Town..." disabled>
<!--                                <select id="ce_city" class="form-control">-->
<!--                                    <option selected disabled>Choose City/Town...</option>-->
<!--                                </select>-->
                            </div>
                            <div class="form-group col-md-2">
                                <label for="ce_pin">Zip<span class="cereq">*</span></label>
                                <input autocomplete="__away" type="text" class="form-control" id="ce_pin">
                            </div>
                        </div>

                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <label>Event Department<span class="cereq">*</span></label>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-3">
                                <?php foreach (array_slice($ce_department,0,12) as $ce_departments): ?>
                                    <div class="form-check">
                                        <input name="evedept[]" class="form-check-input evedept" type="checkbox" value="<?php echo $ce_departments['dept_id']; ?>" id="evedept_<?php echo $ce_departments['dept_id']; ?>">
                                        <label class="form-check-label" for="evedept_<?php echo $ce_departments['dept_id']; ?>">
                                            <?php echo $ce_departments['dept_name']; ?>
                                        </label>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                            <div class="form-group col-md-3">
                                <?php foreach (array_slice($ce_department,12,12) as $ce_departments): ?>
                                    <div class="form-check">
                                        <input name="evedept[]" class="form-check-input evedept" type="checkbox" value="<?php echo $ce_departments['dept_id']; ?>" id="evedept_<?php echo $ce_departments['dept_id']; ?>">
                                        <label class="form-check-label" for="evedept_<?php echo $ce_departments['dept_id']; ?>">
                                            <?php echo $ce_departments['dept_name']; ?>
                                        </label>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                            <div class="form-group col-md-3">
                                <?php foreach (array_slice($ce_department,24,8) as $ce_departments): ?>
                                    <div class="form-check">
                                        <input name="evedept[]" class="form-check-input evedept" type="checkbox" value="<?php echo $ce_departments['dept_id']; ?>" id="evedept_<?php echo $ce_departments['dept_id']; ?>">
                                        <label class="form-check-label" for="evedept_<?php echo $ce_departments['dept_id']; ?>">
                                            <?php echo $ce_departments['dept_name']; ?>
                                        </label>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                            <div class="form-group col-md-3">
                                <?php foreach (array_slice($ce_department,32,6) as $ce_departments): ?>
                                    <div class="form-check">
                                        <input name="evedept[]" class="form-check-input evedept" type="checkbox" value="<?php echo $ce_departments['dept_id']; ?>" id="evedept_<?php echo $ce_departments['dept_id']; ?>">
                                        <label class="form-check-label" for="evedept_<?php echo $ce_departments['dept_id']; ?>">
                                            <?php echo $ce_departments['dept_name']; ?>
                                        </label>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>

                        <hr>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Event Banner</label>
                                <div class="custom-file">
                                    <input type="file" name="ce_banner" class="custom-file-input" id="ce_banner">
                                    <label class="custom-file-label" for="ce_banner">Choose file...</label>
                                    <div class="invalid-feedback">Example invalid custom file feedback</div>
                                    <input type="hidden" id="cebanner">
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <img id="ce_banner_image" style="display: none;" src="#" alt="Event Banner" />
                            </div>
                        </div>

                        <hr>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="ce_evweb">Event Website</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="fas fa-link"></i></div>
                                    </div>
                                    <input autocomplete="__away" type="text" class="form-control" id="ce_evweb" placeholder="http://www.example.com">
                                </div>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="ce_reglink">Registration Link</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="fas fa-link"></i></div>
                                    </div>
                                    <input autocomplete="__away" type="text" class="form-control" id="ce_reglink" placeholder="http://www.example.com">
                                </div>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="ce_clglink">College Website</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="fas fa-link"></i></div>
                                    </div>
                                    <input autocomplete="__away" type="text" class="form-control" id="ce_clglink" placeholder="http://www.example.com">
                                </div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="ce_fb">Facebook Link</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="fas fa-link"></i></div>
                                    </div>
                                    <input autocomplete="__away" type="text" class="form-control" id="ce_fb" placeholder="Facebook URL...">
                                </div>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="ce_tw">Twitter Link</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="fas fa-link"></i></div>
                                    </div>
                                    <input autocomplete="__away" type="text" class="form-control" id="ce_tw" placeholder="Twitter URL...">
                                </div>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="ce_yt">Youtube Link</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="fas fa-link"></i></div>
                                    </div>
                                    <input autocomplete="__away" type="text" class="form-control" id="ce_yt" placeholder="Youtube URL...">
                                </div>
                            </div>
                        </div>

                        <hr>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="ce_ctperson">Contact Person Name<span class="cereq">*</span></label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="fas fa-user"></i></div>
                                    </div>
                                    <input autocomplete="__away" type="text" class="form-control" id="ce_ctperson" placeholder="Contact Person Name...">
                                </div>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="ce_cphone">Contact phone<span class="cereq">*</span></label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="fas fa-phone"></i></div>
                                    </div>
                                    <input autocomplete="__away" type="text" class="form-control" id="ce_cphone" placeholder="+91 9876543210">
                                </div>
                            </div>
                            <div class="form-group col-md-4">
                                <label autocomplete="__away" for="ce_cmail">Contact Email<span class="cereq">*</span></label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="fas fa-at"></i></div>
                                    </div>
                                    <input type="text" class="form-control" id="ce_cmail" placeholder="Email Address...">
                                </div>
                            </div>
                        </div>

                        <div class="text-right">
<!--                            <button type="button" id="ce_submit" class="btn btn-primary">Submit Event</button>-->
                            <input type="submit" value="Submit Event" class="btn btn-primary">
                        </div>

                        <div class="alert alert-success ce_subpro" style="display: none;" role="alert"></div>
                        <div class="alert alert-danger ce_suberr" style="display: none;" role="alert"></div>

                    </form>

                </div>

            </div>
            <!-- /.row -->
        </div>

    </div>
</section>

<!-- /.container -->