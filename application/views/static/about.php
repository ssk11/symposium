<!-- Page Content -->
<div class="container">

    <div class="row">
            <!-- Post Content Column -->
            <div class="col-md-8">

                <h1>About Us</h1>
                <p>Nationalsymposium.in is a <a href="http://www.nationalsymposium.in/">free online portal to share and find all types of college events in India.</a></p>

                <p>The main goal of this portal is to spread all types of college events (Symposium, Workshop, Seminar, Conferences, Sports etc.,) to all college students in India.</p>

            </div>

            <!-- Sidebar Widgets Column -->
            <div class="col-md-4">

                <?php $this->load->view('common/sidebar'); ?>

            </div>

    </div>
    <!-- /.row -->

</div>
<!-- /.container -->