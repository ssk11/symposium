<!-- Page Content -->
<div class="container">

    <div class="row">
        <!-- Post Content Column -->
        <div class="col-md-8">

            <h1 class="statich">Please send your query to us.</h1>
            <hr>
            <form>
                <div class="form-group">
                    <label for="cfullname">Fullname</label>
                    <input type="text" class="form-control" id="cfullname" placeholder="Fullname...">
                </div>
                <div class="form-group">
                    <label for="cemail">Email address</label>
                    <input type="email" class="form-control" id="cemail" placeholder="name@example.com">
                </div>
                <div class="form-group">
                    <label for="cregarding">Regarding</label>
                    <select class="form-control" id="cregarding">
                        <option value="General Query">General Query</option>
                        <option value="Problem in website">Problem in website</option>
                        <option value="Feedback">Feedback</option>
                        <option value="Advertise">Advertise</option>
                        <option value="Others">Others</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="cmsg">Message</label>
                    <textarea class="form-control" id="cmsg" rows="3"></textarea>
                </div>
                <div class="form-group text-right">
                    <button id="sendcmail" type="button" class="btn btn-outline-secondary">Send Message</button>
                </div>
                <div class="alert alert-success" id="csuccess" style="display: none;" role="alert">
                    A simple success alertócheck it out!
                </div>
                <div class="alert alert-danger" id="cfail" style="display: none;" role="alert">
                    A simple danger alertócheck it out!
                </div>
            </form>
        </div>

        <!-- Sidebar Widgets Column -->
        <div class="col-md-4">

            <?php $this->load->view('common/sidebar'); ?>

        </div>

    </div>
    <!-- /.row -->

</div>
<!-- /.container -->