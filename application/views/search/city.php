<!-- Page Content -->
<div class="container">

    <div class="row">

        <!-- Blog Entries Column -->
        <div class="col-md-8">

            <h1 class="my-4 evesubtitle">Find all popular college events in your city/town</h1>

            <ul class="city-alpha justify-content-center mb-4">
                <li><a href="#a">A</a></li>
                <li><a href="#b">B</a></li>
                <li><a href="#c">C</a></li>
                <li><a href="#d">D</a></li>
                <li><a href="#e">E</a></li>
                <li><a href="#f">F</a></li>
                <li><a href="#g">G</a></li>
                <li><a href="#h">H</a></li>
                <li><a href="#i">I</a></li>
                <li><a href="#j">J</a></li>
                <li><a href="#k">K</a></li>
                <li><a href="#l">L</a></li>
                <li><a href="#m">M</a></li>
                <li><a href="#n">N</a></li>
                <li><a href="#o">O</a></li>
                <li><a href="#p">P</a></li>
                <li><a href="#q">Q</a></li>
                <li><a href="#r">R</a></li>
                <li><a href="#s">S</a></li>
                <li><a href="#t">T</a></li>
                <li><a href="#u">U</a></li>
                <li><a href="#v">V</a></li>
                <li><a href="#w">W</a></li>
                <li><a href="#x">X</a></li>
                <li><a href="#y">Y</a></li>
                <li><a href="#z">Z</a></li>
            </ul>
            <div class="clearfix"></div>
            <section class="city-view" id="a">
                <h3>A</h3>
                <ul>
                    <?php foreach ($city_name as $city_names): ?>
                        <?php
                            if(substr( $city_names['city_name'], 0 )[0] == "A") {
                                echo "<li><a href='".base_url()."city/".$city_names['city_id']."/college-events-in-".lcfirst($city_names['city_name'])."'>".$city_names['city_name']."(".$city_names['count(*)'].")"."</a></li>";
                            }
                        ?>
                    <?php endforeach; ?>
                </ul>
                <div class="clearfix"></div>
            </section>
            <hr>
            <section class="city-view" id="b">
                <h3>B</h3>
                <ul>
                    <?php foreach ($city_name as $city_names): ?>
                        <?php
                        if(substr( $city_names['city_name'], 0 )[0] == "B") {
                            echo "<li><a href='".base_url()."city/".$city_names['city_id']."/college-events-in-".lcfirst($city_names['city_name'])."'>".$city_names['city_name']."(".$city_names['count(*)'].")"."</a></li>";
                        }
                        ?>
                    <?php endforeach; ?>
                </ul>
                <div class="clearfix"></div>
            </section>
            <hr>
            <section class="city-view" id="c">
                <h3>C</h3>
                <ul>
                    <?php foreach ($city_name as $city_names): ?>
                        <?php
                        if(substr( $city_names['city_name'], 0 )[0] == "C") {
                            echo "<li><a href='".base_url()."city/".$city_names['city_id']."/college-events-in-".lcfirst($city_names['city_name'])."'>".$city_names['city_name']."(".$city_names['count(*)'].")"."</a></li>";
                        }
                        ?>
                    <?php endforeach; ?>
                </ul>
                <div class="clearfix"></div>
            </section>
            <hr>
            <section class="city-view" id="d">
                <h3>D</h3>
                <ul>
                    <?php foreach ($city_name as $city_names): ?>
                        <?php
                        if(substr( $city_names['city_name'], 0 )[0] == "D") {
                            echo "<li><a href='".base_url()."city/".$city_names['city_id']."/college-events-in-".lcfirst($city_names['city_name'])."'>".$city_names['city_name']."(".$city_names['count(*)'].")"."</a></li>";
                        }
                        ?>
                    <?php endforeach; ?>
                </ul>
                <div class="clearfix"></div>
            </section>
            <hr>
            <section class="city-view" id="e">
                <h3>E</h3>
                <ul>
                    <?php foreach ($city_name as $city_names): ?>
                        <?php
                        if(substr( $city_names['city_name'], 0 )[0] == "E") {
                            echo "<li><a href='".base_url()."city/".$city_names['city_id']."/college-events-in-".lcfirst($city_names['city_name'])."'>".$city_names['city_name']."(".$city_names['count(*)'].")"."</a></li>";
                        }
                        ?>
                    <?php endforeach; ?>
                </ul>
                <div class="clearfix"></div>
            </section>
            <hr>
            <section class="city-view" id="f">
                <h3>F</h3>
                <ul>
                    <?php foreach ($city_name as $city_names): ?>
                        <?php
                        if(substr( $city_names['city_name'], 0 )[0] == "F") {
                            echo "<li><a href='".base_url()."city/".$city_names['city_id']."/college-events-in-".lcfirst($city_names['city_name'])."'>".$city_names['city_name']."(".$city_names['count(*)'].")"."</a></li>";
                        }
                        ?>
                    <?php endforeach; ?>
                </ul>
                <div class="clearfix"></div>
            </section>
            <hr>
            <section class="city-view" id="g">
                <h3>G</h3>
                <ul>
                    <?php foreach ($city_name as $city_names): ?>
                        <?php
                        if(substr( $city_names['city_name'], 0 )[0] == "G") {
                            echo "<li><a href='".base_url()."city/".$city_names['city_id']."/college-events-in-".lcfirst($city_names['city_name'])."'>".$city_names['city_name']."(".$city_names['count(*)'].")"."</a></li>";
                        }
                        ?>
                    <?php endforeach; ?>
                </ul>
                <div class="clearfix"></div>
            </section>
            <hr>
            <section class="city-view" id="h">
                <h3>H</h3>
                <ul>
                    <?php foreach ($city_name as $city_names): ?>
                        <?php
                        if(substr( $city_names['city_name'], 0 )[0] == "H") {
                            echo "<li><a href='".base_url()."city/".$city_names['city_id']."/college-events-in-".lcfirst($city_names['city_name'])."'>".$city_names['city_name']."(".$city_names['count(*)'].")"."</a></li>";
                        }
                        ?>
                    <?php endforeach; ?>
                </ul>
                <div class="clearfix"></div>
            </section>
            <hr>
            <section class="city-view" id="i">
                <h3>I</h3>
                <ul>
                    <?php foreach ($city_name as $city_names): ?>
                        <?php
                        if(substr( $city_names['city_name'], 0 )[0] == "I") {
                            echo "<li><a href='".base_url()."city/".$city_names['city_id']."/college-events-in-".lcfirst($city_names['city_name'])."'>".$city_names['city_name']."(".$city_names['count(*)'].")"."</a></li>";
                        }
                        ?>
                    <?php endforeach; ?>
                </ul>
                <div class="clearfix"></div>
            </section>
            <hr>
            <section class="city-view" id="j">
                <h3>J</h3>
                <ul>
                    <?php foreach ($city_name as $city_names): ?>
                        <?php
                        if(substr( $city_names['city_name'], 0 )[0] == "J") {
                            echo "<li><a href='".base_url()."city/".$city_names['city_id']."/college-events-in-".lcfirst($city_names['city_name'])."'>".$city_names['city_name']."(".$city_names['count(*)'].")"."</a></li>";
                        }
                        ?>
                    <?php endforeach; ?>
                </ul>
                <div class="clearfix"></div>
            </section>
            <hr>
            <section class="city-view" id="k">
                <h3>K</h3>
                <ul>
                    <?php foreach ($city_name as $city_names): ?>
                        <?php
                        if(substr( $city_names['city_name'], 0 )[0] == "K") {
                            echo "<li><a href='".base_url()."city/".$city_names['city_id']."/college-events-in-".lcfirst($city_names['city_name'])."'>".$city_names['city_name']."(".$city_names['count(*)'].")"."</a></li>";
                        }
                        ?>
                    <?php endforeach; ?>
                </ul>
                <div class="clearfix"></div>
            </section>
            <hr>
            <section class="city-view" id="l">
                <h3>L</h3>
                <ul>
                    <?php foreach ($city_name as $city_names): ?>
                        <?php
                        if(substr( $city_names['city_name'], 0 )[0] == "L") {
                            echo "<li><a href='".base_url()."city/".$city_names['city_id']."/college-events-in-".lcfirst($city_names['city_name'])."'>".$city_names['city_name']."(".$city_names['count(*)'].")"."</a></li>";
                        }
                        ?>
                    <?php endforeach; ?>
                </ul>
                <div class="clearfix"></div>
            </section>
            <hr>
            <section class="city-view" id="m">
                <h3>M</h3>
                <ul>
                    <?php foreach ($city_name as $city_names): ?>
                        <?php
                        if(substr( $city_names['city_name'], 0 )[0] == "M") {
                            echo "<li><a href='".base_url()."city/".$city_names['city_id']."/college-events-in-".lcfirst($city_names['city_name'])."'>".$city_names['city_name']."(".$city_names['count(*)'].")"."</a></li>";
                        }
                        ?>
                    <?php endforeach; ?>
                </ul>
                <div class="clearfix"></div>
            </section>
            <hr>
            <section class="city-view" id="n">
                <h3>N</h3>
                <ul>
                    <?php foreach ($city_name as $city_names): ?>
                        <?php
                        if(substr( $city_names['city_name'], 0 )[0] == "N") {
                            echo "<li><a href='".base_url()."city/".$city_names['city_id']."/college-events-in-".lcfirst($city_names['city_name'])."'>".$city_names['city_name']."(".$city_names['count(*)'].")"."</a></li>";
                        }
                        ?>
                    <?php endforeach; ?>
                </ul>
                <div class="clearfix"></div>
            </section>
            <hr>
            <section class="city-view" id="o">
                <h3>O</h3>
                <ul>
                    <?php foreach ($city_name as $city_names): ?>
                        <?php
                        if(substr( $city_names['city_name'], 0 )[0] == "O") {
                            echo "<li><a href='".base_url()."city/".$city_names['city_id']."/college-events-in-".lcfirst($city_names['city_name'])."'>".$city_names['city_name']."(".$city_names['count(*)'].")"."</a></li>";
                        }
                        ?>
                    <?php endforeach; ?>
                </ul>
                <div class="clearfix"></div>
            </section>
            <hr>
            <section class="city-view" id="p">
                <h3>P</h3>
                <ul>
                    <?php foreach ($city_name as $city_names): ?>
                        <?php
                        if(substr( $city_names['city_name'], 0 )[0] == "P") {
                            echo "<li><a href='".base_url()."city/".$city_names['city_id']."/college-events-in-".lcfirst($city_names['city_name'])."'>".$city_names['city_name']."(".$city_names['count(*)'].")"."</a></li>";
                        }
                        ?>
                    <?php endforeach; ?>
                </ul>
                <div class="clearfix"></div>
            </section>
            <hr>
            <section class="city-view" id="q">
                <h3>Q</h3>
                <ul>
                    <?php foreach ($city_name as $city_names): ?>
                        <?php
                        if(substr( $city_names['city_name'], 0 )[0] == "Q") {
                            echo "<li><a href='".base_url()."city/".$city_names['city_id']."/college-events-in-".lcfirst($city_names['city_name'])."'>".$city_names['city_name']."(".$city_names['count(*)'].")"."</a></li>";
                        }
                        ?>
                    <?php endforeach; ?>
                </ul>
                <div class="clearfix"></div>
            </section>
            <hr>
            <section class="city-view" id="r">
                <h3>R</h3>
                <ul>
                    <?php foreach ($city_name as $city_names): ?>
                        <?php
                        if(substr( $city_names['city_name'], 0 )[0] == "R") {
                            echo "<li><a href='".base_url()."city/".$city_names['city_id']."/college-events-in-".lcfirst($city_names['city_name'])."'>".$city_names['city_name']."(".$city_names['count(*)'].")"."</a></li>";
                        }
                        ?>
                    <?php endforeach; ?>
                </ul>
                <div class="clearfix"></div>
            </section>
            <hr>
            <section class="city-view" id="s">
                <h3>S</h3>
                <ul>
                    <?php foreach ($city_name as $city_names): ?>
                        <?php
                        if(substr( $city_names['city_name'], 0 )[0] == "S") {
                            echo "<li><a href='".base_url()."city/".$city_names['city_id']."/college-events-in-".lcfirst($city_names['city_name'])."'>".$city_names['city_name']."(".$city_names['count(*)'].")"."</a></li>";
                        }
                        ?>
                    <?php endforeach; ?>
                </ul>
                <div class="clearfix"></div>
            </section>
            <hr>
            <section class="city-view" id="t">
                <h3>T</h3>
                <ul>
                    <?php foreach ($city_name as $city_names): ?>
                        <?php
                        if(substr( $city_names['city_name'], 0 )[0] == "T") {
                            echo "<li><a href='".base_url()."city/".$city_names['city_id']."/college-events-in-".lcfirst($city_names['city_name'])."'>".$city_names['city_name']."(".$city_names['count(*)'].")"."</a></li>";
                        }
                        ?>
                    <?php endforeach; ?>
                </ul>
                <div class="clearfix"></div>
            </section>
            <hr>
            <section class="city-view" id="u">
                <h3>U</h3>
                <ul>
                    <?php foreach ($city_name as $city_names): ?>
                        <?php
                        if(substr( $city_names['city_name'], 0 )[0] == "U") {
                            echo "<li><a href='".base_url()."city/".$city_names['city_id']."/college-events-in-".lcfirst($city_names['city_name'])."'>".$city_names['city_name']."(".$city_names['count(*)'].")"."</a></li>";
                        }
                        ?>
                    <?php endforeach; ?>
                </ul>
                <div class="clearfix"></div>
            </section>
            <hr>
            <section class="city-view" id="v">
                <h3>V</h3>
                <ul>
                    <?php foreach ($city_name as $city_names): ?>
                        <?php
                        if(substr( $city_names['city_name'], 0 )[0] == "V") {
                            echo "<li><a href='".base_url()."city/".$city_names['city_id']."/college-events-in-".lcfirst($city_names['city_name'])."'>".$city_names['city_name']."(".$city_names['count(*)'].")"."</a></li>";
                        }
                        ?>
                    <?php endforeach; ?>
                </ul>
                <div class="clearfix"></div>
            </section>
            <hr>
            <section class="city-view" id="w">
                <h3>W</h3>
                <ul>
                    <?php foreach ($city_name as $city_names): ?>
                        <?php
                        if(substr( $city_names['city_name'], 0 )[0] == "W") {
                            echo "<li><a href='".base_url()."city/".$city_names['city_id']."/college-events-in-".lcfirst($city_names['city_name'])."'>".$city_names['city_name']."(".$city_names['count(*)'].")"."</a></li>";
                        }
                        ?>
                    <?php endforeach; ?>
                </ul>
                <div class="clearfix"></div>
            </section>
            <hr>
            <section class="city-view" id="x">
                <h3>X</h3>
                <ul>
                    <?php foreach ($city_name as $city_names): ?>
                        <?php
                        if(substr( $city_names['city_name'], 0 )[0] == "X") {
                            echo "<li><a href='".base_url()."city/".$city_names['city_id']."/college-events-in-".lcfirst($city_names['city_name'])."'>".$city_names['city_name']."(".$city_names['count(*)'].")"."</a></li>";
                        }
                        ?>
                    <?php endforeach; ?>
                </ul>
                <div class="clearfix"></div>
            </section>
            <hr>
            <section class="city-view" id="y">
                <h3>Y</h3>
                <ul>
                    <?php foreach ($city_name as $city_names): ?>
                        <?php
                        if(substr( $city_names['city_name'], 0 )[0] == "Y") {
                            echo "<li><a href='".base_url()."city/".$city_names['city_id']."/college-events-in-".lcfirst($city_names['city_name'])."'>".$city_names['city_name']."(".$city_names['count(*)'].")"."</a></li>";
                        }
                        ?>
                    <?php endforeach; ?>
                </ul>
                <div class="clearfix"></div>
            </section>
            <hr>
            <section class="city-view" id="z">
                <h3>Z</h3>
                <ul>
                    <?php foreach ($city_name as $city_names): ?>
                        <?php
                        if(substr( $city_names['city_name'], 0 )[0] == "Z") {
                            echo "<li><a href='".base_url()."city/".$city_names['city_id']."/college-events-in-".lcfirst($city_names['city_name'])."'>".$city_names['city_name']."(".$city_names['count(*)'].")"."</a></li>";
                        }
                        ?>
                    <?php endforeach; ?>
                </ul>
                <div class="clearfix"></div>
            </section>
            <hr>


        </div>

        <!-- Sidebar Widgets Column -->
        <div class="col-md-4">

            <?php $this->load->view('common/sidebar'); ?>

        </div>

    </div>
    <!-- /.row -->

</div>
<!-- /.container -->