<!-- Page Content -->
<div class="container">

    <div class="row">

        <!-- Blog Entries Column -->
        <div class="col-md-8">

            <h1 class="my-4 evesubtitle">Find all popular college events in your departments</h1>
            <div class="row">
                <div class="col-md-6">
                    <ul class="list-group stateul">
                        <?php foreach (array_slice($department_name,0,12) as $firstColumn): ?>
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            <a href="<?php echo base_url('event-departments/'.$firstColumn["dept_id"].'/'.strtolower(str_replace(" ","-",$firstColumn["dept_name"])).''); ?>"><?php echo $firstColumn['dept_name']; ?></a>
                        </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <div class="col-md-6">
                    <ul class="list-group stateul">
                        <?php foreach (array_slice($department_name,12,12) as $secondColumn): ?>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                <a href="<?php echo base_url('event-departments/'.$secondColumn["dept_id"].'/'.strtolower(str_replace(" ","-",$secondColumn["dept_name"])).''); ?>"><?php echo $secondColumn['dept_name']; ?></a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <ul class="list-group stateul">
                        <?php foreach (array_slice($department_name,18,10) as $thirdColumn): ?>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                <a href="<?php echo base_url('event-departments/'.$thirdColumn["dept_id"].'/'.strtolower(str_replace(" ","-",$thirdColumn["dept_name"])).''); ?>"><?php echo $thirdColumn['dept_name']; ?></a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <div class="col-md-6">
                    <ul class="list-group stateul">
                        <?php foreach (array_slice($department_name,28,10) as $fourthColumn): ?>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                <a href="<?php echo base_url('event-departments/'.$fourthColumn["dept_id"].'/'.strtolower(str_replace(" ","-",$fourthColumn["dept_name"])).''); ?>"><?php echo $fourthColumn['dept_name']; ?></a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>

        </div>

        <!-- Sidebar Widgets Column -->
        <div class="col-md-4">

            <?php $this->load->view('common/sidebar'); ?>

        </div>

    </div>
    <!-- /.row -->

</div>
<!-- /.container -->