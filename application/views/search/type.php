<!-- Page Content -->
<div class="container">

    <div class="row">

        <!-- Blog Entries Column -->
        <div class="col-md-8">

            <h1 class="my-4 evesubtitle">Find all popular college events</h1>
            <div class="row">
                <div class="col-md-6">
                    <ul class="list-group stateul">
                        <?php foreach (array_slice($type_name,0,10) as $firstColumn): ?>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                <a href="<?php echo base_url('event-types/'.$firstColumn["type_id"].'/'.strtolower(str_replace(" ","-",$firstColumn["type_name"])).''); ?>"><?php echo $firstColumn['type_name']; ?></a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <div class="col-md-6">
                    <ul class="list-group stateul">
                        <?php foreach (array_slice($type_name,10,11) as $secondColumn): ?>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                <a href="<?php echo base_url('event-types/'.$secondColumn["type_id"].'/'.strtolower(str_replace(" ","-",$secondColumn["type_name"])).''); ?>"><?php echo $secondColumn['type_name']; ?></a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </div>

        <!-- Sidebar Widgets Column -->
        <div class="col-md-4">

            <?php $this->load->view('common/sidebar'); ?>

        </div>

    </div>
    <!-- /.row -->

</div>
<!-- /.container -->