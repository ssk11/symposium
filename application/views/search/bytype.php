<!-- Page Content -->
<div class="container">

    <div class="row">

        <!-- Blog Entries Column -->
        <div class="col-md-8">
            <?php
            if(!empty($latest_event)) {
            ?>
            <h1 class="my-4 evesubtitle">Upcoming <?php echo $latest_event[0]['type_name']; ?> Events</h1>
            <?php } ?>
            <?php
            if(empty($latest_event)) {
                ?>
                <h6>Sorry no events found right now..</h6>
                <?php
            } else {
                ?>
                <!-- Blog Post -->
                <?php foreach ($latest_event as $latest_event): ?>
                    <div class="card mb-4">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <?php
                                    if($latest_event['eve_banner'] == ""){
                                        ?>
                                        <img class="img-fluid rounded" src="" width="100%" height="auto" alt="">
                                        <a href="<?php echo base_url(); ?>e/<?php echo $latest_event['eve_id']; ?>/<?php echo $latest_event['eve_slug']; ?>"><img src="<?php echo base_url('uploads/cover-image/default-cover-image.jpg') ?>" alt="Card image cap" width="100%" height="auto"></a>
                                        <?php
                                    } else {
                                        ?>
                                        <a href="<?php echo base_url(); ?>e/<?php echo $latest_event['eve_id']; ?>/<?php echo $latest_event['eve_slug']; ?>"><img src="<?php echo $latest_event['eve_banner']; ?>" alt="Card image cap" width="100%" height="auto"></a>
                                        <?php
                                    }
                                    ?>
                                </div>
                                <div class="col-md-9">
                                    <h2 class="card-title"><a href="<?php echo base_url(); ?>e/<?php echo $latest_event['eve_id']; ?>/<?php echo $latest_event['eve_slug']; ?>"><?php echo $latest_event['eve_title']; ?></a></h2>
                                    <p class="card-text"><?php echo $latest_event['eve_clg']; ?>, <?php echo $latest_event['eve_city']; ?>,<?php echo $latest_event['state_name']; ?></p>
                                    <div class="text-muted">
                                        <?php
                                        $postdate = $latest_event['eve_postdate'];
                                        $y = date('Y',strtotime($postdate));
                                        $d = date('d',strtotime($postdate));
                                        $m = date('M',strtotime($postdate));
                                        ?>
                                        Posted on <?php echo $m; ?> <?php echo $d; ?>, <?php echo $y; ?> by
                                        <a href="#"><?php echo $latest_event['eve_puser']; ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; } ?>
            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <ins class="adsbygoogle"
                 style="display:block"
                 data-ad-format="fluid"
                 data-ad-layout-key="-f9+5v+4m-d8+7b"
                 data-ad-client="ca-pub-3342812138107938"
                 data-ad-slot="9434684583"></ins>
            <script>
                (adsbygoogle = window.adsbygoogle || []).push({});
            </script>

            <!-- Pagination -->
            <?php if (isset($links)) { ?>
                <?php echo $links ?>
            <?php } ?>

        </div>

        <!-- Sidebar Widgets Column -->
        <div class="col-md-4">

            <?php $this->load->view('common/sidebar'); ?>
        </div>

    </div>
    <!-- /.row -->

</div>
<!-- /.container -->