<!-- Page Content -->
<div class="container">

    <div class="row">

        <!-- Blog Entries Column -->
        <div class="col-md-8">

            <h1 class="my-4 evesubtitle">Find all popular college events in your state</h1>
            <div class="row">
                <div class="col-md-6">
                    <ul class="list-group stateul">
                        <li class="list-group-item d-flex align-items-center statetitle">
                            <i class="fas fa-map-marker-alt"></i>&nbsp;College Events in South India
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            <a href="<?php echo base_url('state/1/college-events-in-andhra-pradesh'); ?>">College Events in Andhra Pradesh</a>
                            <?php
                            if(!empty($state_name) && array_search(1, array_column($state_name, 'state_id')) !== False) {
                                ?>
                                <span class="badge badge-primary badge-pill">
                                <?php
                                $key = array_search(1, array_column($state_name, 'state_id'));
                                echo $state_name[$key]['count(*)'];
                                ?>
                            </span>
                            <?php } ?>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            <a href="<?php echo base_url('state/12/college-events-in-karnataka'); ?>">College Events in Karnataka</a>
                            <?php
                            if(!empty($state_name) && array_search(12, array_column($state_name, 'state_id')) !== False) {
                                ?>
                                <span class="badge badge-primary badge-pill">
                                <?php
                                $key = array_search(12, array_column($state_name, 'state_id'));
                                echo $state_name[$key]['count(*)'];
                                ?>
                            </span>
                            <?php } ?>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            <a href="<?php echo base_url('state/13/college-events-in-kerala'); ?>">College Events in Kerala</a>
                            <?php
                            if(!empty($state_name) && array_search(13, array_column($state_name, 'state_id')) !== False) {
                                ?>
                                <span class="badge badge-primary badge-pill">
                                <?php
                                $key = array_search(13, array_column($state_name, 'state_id'));
                                echo $state_name[$key]['count(*)'];
                                ?>
                            </span>
                            <?php } ?>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            <a href="<?php echo base_url('state/24/college-events-in-tamil-nadu'); ?>">College Events in Tamil Nadu</a>
                            <?php
                            if(!empty($state_name) && array_search(24, array_column($state_name, 'state_id')) !== False) {
                            ?>
                            <span class="badge badge-primary badge-pill">
                                <?php
                                $key = array_search(24, array_column($state_name, 'state_id'));
                                echo $state_name[$key]['count(*)'];
                                ?>
                            </span>
                            <?php } ?>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            <a href="<?php echo base_url('state/25/college-events-in-telangana'); ?>">College Events in Telangana</a>
                            <?php
                            if(!empty($state_name) && array_search(25, array_column($state_name, 'state_id')) !== False) {
                                ?>
                                <span class="badge badge-primary badge-pill">
                                <?php
                                $key = array_search(25, array_column($state_name, 'state_id'));
                                echo $state_name[$key]['count(*)'];
                                ?>
                            </span>
                            <?php } ?>
                        </li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <ul class="list-group stateul">
                        <li class="list-group-item d-flex align-items-center statetitle">
                            <i class="fas fa-map-marker-alt"></i>&nbsp;College Events in West India
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            <a href="<?php echo base_url('state/6/college-events-in-goa'); ?>">College Events in Goa</a>
                            <?php
                            if(!empty($state_name) && array_search(6, array_column($state_name, 'state_id')) !== False) {
                                ?>
                                <span class="badge badge-primary badge-pill">
                                <?php
                                $key = array_search(6, array_column($state_name, 'state_id'));
                                echo $state_name[$key]['count(*)'];
                                ?>
                            </span>
                            <?php } ?>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            <a href="<?php echo base_url('state/7/college-events-in-gujarat'); ?>">College Events in Gujarat</a>
                            <?php
                            if(!empty($state_name) && array_search(7, array_column($state_name, 'state_id')) !== False) {
                                ?>
                                <span class="badge badge-primary badge-pill">
                                <?php
                                $key = array_search(7, array_column($state_name, 'state_id'));
                                echo $state_name[$key]['count(*)'];
                                ?>
                            </span>
                            <?php } ?>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            <a href="<?php echo base_url('state/14/college-events-in-madhya-pradesh'); ?>">College Events in Madhya Pradesh</a>
                            <?php
                            if(!empty($state_name) && array_search(14, array_column($state_name, 'state_id')) !== False) {
                                ?>
                                <span class="badge badge-primary badge-pill">
                                <?php
                                $key = array_search(14, array_column($state_name, 'state_id'));
                                echo $state_name[$key]['count(*)'];
                                ?>
                            </span>
                            <?php } ?>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            <a href="<?php echo base_url('state/15/college-events-in-maharashtra'); ?>">College Events in Maharashtra</a>
                            <?php
                            if(!empty($state_name) && array_search(15, array_column($state_name, 'state_id')) !== False) {
                                ?>
                                <span class="badge badge-primary badge-pill">
                                <?php
                                $key = array_search(15, array_column($state_name, 'state_id'));
                                echo $state_name[$key]['count(*)'];
                                ?>
                            </span>
                            <?php } ?>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            <a href="<?php echo base_url('state/22/college-events-in-rajasthan'); ?>">College Events in Rajasthan</a>
                            <?php
                            if(!empty($state_name) && array_search(22, array_column($state_name, 'state_id')) !== False) {
                                ?>
                                <span class="badge badge-primary badge-pill">
                                <?php
                                $key = array_search(22, array_column($state_name, 'state_id'));
                                echo $state_name[$key]['count(*)'];
                                ?>
                            </span>
                            <?php } ?>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <ul class="list-group stateul">
                        <li class="list-group-item d-flex align-items-center statetitle">
                            <i class="fas fa-map-marker-alt"></i>&nbsp;College Events in North India
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            <a href="<?php echo base_url('state/30/college-events-in-delhi'); ?>">College Events in Delhi</a>
                            <?php
                            if(!empty($state_name) && array_search(30, array_column($state_name, 'state_id')) !== False) {
                                ?>
                                <span class="badge badge-primary badge-pill">
                                <?php
                                $key = array_search(30, array_column($state_name, 'state_id'));
                                echo $state_name[$key]['count(*)'];
                                ?>
                            </span>
                            <?php } ?>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            <a href="<?php echo base_url('state/8/college-events-in-haryana'); ?>">College Events in Haryana</a>
                            <?php
                            if(!empty($state_name) && array_search(8, array_column($state_name, 'state_id')) !== False) {
                                ?>
                                <span class="badge badge-primary badge-pill">
                                <?php
                                $key = array_search(8, array_column($state_name, 'state_id'));
                                echo $state_name[$key]['count(*)'];
                                ?>
                            </span>
                            <?php } ?>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            <a href="<?php echo base_url('state/9/college-events-in-himachal-pradesh'); ?>">College Events in Himachal Pradesh</a>
                            <?php
                            if(!empty($state_name) && array_search(9, array_column($state_name, 'state_id')) !== False) {
                                ?>
                                <span class="badge badge-primary badge-pill">
                                <?php
                                $key = array_search(9, array_column($state_name, 'state_id'));
                                echo $state_name[$key]['count(*)'];
                                ?>
                            </span>
                            <?php } ?>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            <a href="<?php echo base_url('state/21/college-events-in-punjab'); ?>">College Events in Punjab</a>
                            <?php
                            if(!empty($state_name) && array_search(21, array_column($state_name, 'state_id')) !== False) {
                                ?>
                                <span class="badge badge-primary badge-pill">
                                <?php
                                $key = array_search(21, array_column($state_name, 'state_id'));
                                echo $state_name[$key]['count(*)'];
                                ?>
                            </span>
                            <?php } ?>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            <a href="<?php echo base_url('state/28/college-events-in-uttarpradesh'); ?>">College Events in Uttar Pradesh</a>
                            <?php
                            if(!empty($state_name) && array_search(28, array_column($state_name, 'state_id')) !== False) {
                                ?>
                                <span class="badge badge-primary badge-pill">
                                <?php
                                $key = array_search(28, array_column($state_name, 'state_id'));
                                echo $state_name[$key]['count(*)'];
                                ?>
                            </span>
                            <?php } ?>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            <a href="<?php echo base_url('state/27/college-events-in-uttarakhand'); ?>">College Events in Uttarakhand</a>
                            <?php
                            if(!empty($state_name) && array_search(27, array_column($state_name, 'state_id')) !== False) {
                                ?>
                                <span class="badge badge-primary badge-pill">
                                <?php
                                $key = array_search(27, array_column($state_name, 'state_id'));
                                echo $state_name[$key]['count(*)'];
                                ?>
                            </span>
                            <?php } ?>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            <a href="<?php echo base_url('state/10/college-events-in-jammu-and-kashmir'); ?>">College Events in Jammu and Kashmir</a>
                            <?php
                            if(!empty($state_name) && array_search(10, array_column($state_name, 'state_id')) !== False) {
                                ?>
                                <span class="badge badge-primary badge-pill">
                                <?php
                                $key = array_search(10, array_column($state_name, 'state_id'));
                                echo $state_name[$key]['count(*)'];
                                ?>
                            </span>
                            <?php } ?>
                        </li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <ul class="list-group stateul">
                        <li class="list-group-item d-flex align-items-center statetitle">
                            <i class="fas fa-map-marker-alt"></i>&nbsp;College Events in East India
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            <a href="<?php echo base_url('state/3/college-events-in-assam'); ?>">College Events in Assam</a>
                            <?php
                            if(!empty($state_name) && array_search(3, array_column($state_name, 'state_id')) !== False) {
                                ?>
                                <span class="badge badge-primary badge-pill">
                                <?php
                                $key = array_search(3, array_column($state_name, 'state_id'));
                                echo $state_name[$key]['count(*)'];
                                ?>
                            </span>
                            <?php } ?>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            <a href="<?php echo base_url('state/4/college-events-in-bihar'); ?>">College Events in Bihar</a>
                            <?php
                            if(!empty($state_name) && array_search(4, array_column($state_name, 'state_id')) !== False) {
                                ?>
                                <span class="badge badge-primary badge-pill">
                                <?php
                                $key = array_search(4, array_column($state_name, 'state_id'));
                                echo $state_name[$key]['count(*)'];
                                ?>
                            </span>
                            <?php } ?>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            <a href="<?php echo base_url('state/5/college-events-in-chhattisgarh'); ?>">College Events in Chhattisgarh</a>
                            <?php
                            if(!empty($state_name) && array_search(5, array_column($state_name, 'state_id')) !== False) {
                                ?>
                                <span class="badge badge-primary badge-pill">
                                <?php
                                $key = array_search(5, array_column($state_name, 'state_id'));
                                echo $state_name[$key]['count(*)'];
                                ?>
                            </span>
                            <?php } ?>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            <a href="<?php echo base_url('state/11/college-events-in-jharkand'); ?>">College Events in Jharkand</a>
                            <?php
                            if(!empty($state_name) && array_search(11, array_column($state_name, 'state_id')) !== False) {
                                ?>
                                <span class="badge badge-primary badge-pill">
                                <?php
                                $key = array_search(11, array_column($state_name, 'state_id'));
                                echo $state_name[$key]['count(*)'];
                                ?>
                            </span>
                            <?php } ?>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            <a href="<?php echo base_url('state/20/college-events-in-odisha'); ?>">College Events in Odisha</a>
                            <?php
                            if(!empty($state_name) && array_search(20, array_column($state_name, 'state_id')) !== False) {
                                ?>
                                <span class="badge badge-primary badge-pill">
                                <?php
                                $key = array_search(20, array_column($state_name, 'state_id'));
                                echo $state_name[$key]['count(*)'];
                                ?>
                            </span>
                            <?php } ?>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            <a href="<?php echo base_url('state/26/college-events-in-tripura'); ?>">College Events in Tripura</a>
                            <?php
                            if(!empty($state_name) && array_search(26, array_column($state_name, 'state_id')) !== False) {
                                ?>
                                <span class="badge badge-primary badge-pill">
                                <?php
                                $key = array_search(26, array_column($state_name, 'state_id'));
                                echo $state_name[$key]['count(*)'];
                                ?>
                            </span>
                            <?php } ?>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            <a href="<?php echo base_url('state/29/college-events-in-west-bengal'); ?>">College Events in West Bengal</a>
                            <?php
                            if(!empty($state_name) && array_search(29, array_column($state_name, 'state_id')) !== False) {
                                ?>
                                <span class="badge badge-primary badge-pill">
                                <?php
                                $key = array_search(29, array_column($state_name, 'state_id'));
                                echo $state_name[$key]['count(*)'];
                                ?>
                            </span>
                            <?php } ?>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            <a href="<?php echo base_url('state/2/college-events-in-arunachal-pradesh'); ?>">College Events in Arunachal Pradesh</a>
                            <?php
                            if(!empty($state_name) && array_search(2, array_column($state_name, 'state_id')) !== False) {
                                ?>
                                <span class="badge badge-primary badge-pill">
                                <?php
                                $key = array_search(2, array_column($state_name, 'state_id'));
                                echo $state_name[$key]['count(*)'];
                                ?>
                            </span>
                            <?php } ?>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            <a href="<?php echo base_url('state/16/college-events-in-manipur'); ?>">College Events in Manipur</a>
                            <?php
                            if(!empty($state_name) && array_search(16, array_column($state_name, 'state_id')) !== False) {
                                ?>
                                <span class="badge badge-primary badge-pill">
                                <?php
                                $key = array_search(16, array_column($state_name, 'state_id'));
                                echo $state_name[$key]['count(*)'];
                                ?>
                            </span>
                            <?php } ?>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            <a href="<?php echo base_url('state/18/college-events-in-mizoram'); ?>">College Events in Mizoram</a>
                            <?php
                            if(!empty($state_name) && array_search(18, array_column($state_name, 'state_id')) !== False) {
                                ?>
                                <span class="badge badge-primary badge-pill">
                                <?php
                                $key = array_search(18, array_column($state_name, 'state_id'));
                                echo $state_name[$key]['count(*)'];
                                ?>
                            </span>
                            <?php } ?>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            <a href="<?php echo base_url('state/17/college-events-in-meghalaya'); ?>">College Events in Meghalaya</a>
                            <?php
                            if(!empty($state_name) && array_search(17, array_column($state_name, 'state_id')) !== False) {
                                ?>
                                <span class="badge badge-primary badge-pill">
                                <?php
                                $key = array_search(17, array_column($state_name, 'state_id'));
                                echo $state_name[$key]['count(*)'];
                                ?>
                            </span>
                            <?php } ?>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            <a href="<?php echo base_url('state/19/college-events-in-nagaland'); ?>">College Events in Nagaland</a>
                            <?php
                            if(!empty($state_name) && array_search(19, array_column($state_name, 'state_id')) !== False) {
                                ?>
                                <span class="badge badge-primary badge-pill">
                                <?php
                                $key = array_search(19, array_column($state_name, 'state_id'));
                                echo $state_name[$key]['count(*)'];
                                ?>
                            </span>
                            <?php } ?>
                        </li>
                    </ul>
                </div>
            </div>

        </div>

        <!-- Sidebar Widgets Column -->
        <div class="col-md-4">

            <?php $this->load->view('common/sidebar'); ?>

        </div>

    </div>
    <!-- /.row -->

</div>
<!-- /.container -->