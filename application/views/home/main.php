<style>
    .gsensevertical {
        display: none;
    }
</style>
<section class="maintop">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>SEARCH AND SHARE <br><span>ALL COLLEGE EVENTS IN INDIA</span></h1>
                <p>Search Events By</p>
                <a href="<?=base_url("event-types")?>" class="btn btn-outline-light">TYPE</a>
                <a href="<?=base_url("event-departments")?>" class="btn btn-outline-light">DEPARTMENT</a>
                <a href="<?=base_url("college-events-by-city")?>" class="btn btn-outline-light">CITY</a>
                <a href="<?=base_url("college-events-by-state")?>" class="btn btn-outline-light">STATE</a>
            </div>
        </div>
    </div>
</section>
<section class="mainsecond">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        Latest Events
                    </div>
                    <ul class="list-group list-group-flush">
                        <?php foreach ($latest_event as $latest_event): ?>
                        <li class="list-group-item">
                            <a href="<?php echo base_url(); ?>e/<?php echo $latest_event['eve_id']; ?>/<?php echo $latest_event['eve_slug']; ?>">
                                <h3><?php echo $latest_event['eve_title']; ?></h3>
                                <p><?php echo $latest_event['eve_clg']; ?>, <?php echo $latest_event['eve_city']; ?>,<?php echo $latest_event['state_name']; ?></p>
                            </a>
                        </li>
                        <?php endforeach; ?>
                        <li class="list-group-item">
                            <a href="<?php echo base_url("latest-events"); ?>">
                                <h3>View all latest events...</h3>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                        <!-- NS_sidebar -->
                        <ins class="adsbygoogle"
                             style="display:block"
                             data-ad-client="ca-pub-3342812138107938"
                             data-ad-slot="4492660736"
                             data-ad-format="auto"
                             data-full-width-responsive="true"></ins>
                        <script>
                            (adsbygoogle = window.adsbygoogle || []).push({});
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="maintrd">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4>Promote your events and get more audience for your events</h4>
                <a href="<?=base_url()?>create-event" class="btn btn-warning btn-lg">Create Event</a>
            </div>
        </div>
    </div>
</section>
<section class="mainfr">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <h2>
                    <i class="fas fa-user-plus"></i><br>
                    Register
                </h2>
            </div>
            <div class="col-md-4">
                <h2>
                    <i class="fas fa-edit"></i><br>
                    Create Events
                </h2>
            </div>
            <div class="col-md-4">
                <h2>
                    <i class="fas fa-users"></i><br>
                    Get Audience
                </h2>
            </div>
        </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="gsensevertical">
                        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
         NS_bottomad
        <ins class="adsbygoogle"
             style="display:inline-block;width:728px;height:90px"
             data-ad-client="ca-pub-3342812138107938"
             data-ad-slot="4947122153"></ins>
        <script>
        (adsbygoogle = window.adsbygoogle || []).push({});
        </script>
                    </div>
                </div>
            </div>
    </div>
</section>