<section class="consectifon">
    <div class="container">

        <div class="row">
                <div class="col-lg-3">
                    <?php foreach ($basic_info as $basic_pic): ?>
                    <div class="proimg">
                        <?php
                        if($basic_pic['propic']==""){
                            ?>
                            <img src="<?=base_url()?>uploads/profile-image/profile_default.jpg" alt="Card image cap">
                            <?php
                        } else {
                            ?>
                            <img src="<?php echo $basic_pic['propic']; ?>" alt="Card image cap">
                            <?php
                        }
                        ?>
                    </div>
                    <?php endforeach; ?>
                    <div class="list-group">
                        <a href="<?=base_url()?>profile" class="list-group-item">Profile</a>
                        <a href="<?=base_url()?>edit-profile" class="list-group-item active">Edit Profile</a>
                        <a href="<?=base_url()?>update-password" class="list-group-item">Change Password</a>
                        <a href="<?=base_url()?>published-events" class="list-group-item">Published Events</a>
                        <a href="<?=base_url()?>under-review-events" class="list-group-item">Under Review Events</a>
                        <a href="<?=base_url()?>rejected-events" class="list-group-item">Rejected Events</a>
                    </div>
                </div>
                <!-- /.col-lg-3 -->

                <div class="col-lg-9">
                    <?php foreach ($basic_info as $basic_info): ?>
                    <div class="profilesec">
                        <form>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="pro_name">Fullname</label>
                                    <?php
                                    if($basic_info['fullname']==""){
                                        ?>
                                        <input type="text" class="form-control" id="pro_name" placeholder="Fullname">
                                        <?php
                                    } else {
                                        ?>
                                        <input type="text" value="<?php echo $basic_info['fullname']; ?>" class="form-control" id="pro_name" placeholder="Fullname">
                                        <?php
                                    }
                                    ?>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="pro_mobile">Mobile Number</label>
                                    <?php
                                    if($basic_info['mobile']==""){
                                        ?>
                                        <input id="pro_mobile" type="text" class="form-control" placeholder="Mobile number">
                                        <?php
                                    } else {
                                        ?>
                                        <input id="pro_mobile" value="<?php echo $basic_info['mobile']; ?>" type="text" class="form-control" placeholder="Mobile number">
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="pro_gender">Gender</label>
                                    <?php
                                    if($basic_info['gender']==""){
                                        ?>
                                        <select id="pro_gender" class="form-control">
                                            <option disabled selected>Choose your Gender</option>
                                            <option value="Male">Male</option>
                                            <option value="Female">Female</option>
                                            <option value="Other">Other</option>
                                        </select>
                                        <?php
                                    } else {
                                        ?>
                                        <select id="pro_gender" class="form-control">
                                            <option value="<?php echo $basic_info['gender']; ?>" selected><?php echo $basic_info['gender']; ?></option>
                                            <option value="Male">Male</option>
                                            <option value="Female">Female</option>
                                            <option value="Other">Other</option>
                                        </select>
                                        <?php
                                    }
                                    ?>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="pro_clg">College</label>
                                    <input type="text" class="form-control" id="pro_clg" placeholder="College Name">
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="pro_state">State</label>
                                    <select id="pro_state" class="form-control">
                                        <option selected disabled>Choose State...</option>
                                        <?php foreach ($ce_allstates as $ce_showstates): ?>
                                            <option value="<?php echo $ce_showstates['state_id']; ?>"><?php echo $ce_showstates['state_name']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="pro_city">City</label>
                                    <select id="pro_city" class="form-control">
                                        <option selected disabled>Choose City/Town...</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="pro_fb">Facebbok</label>
                                    <?php
                                    if($basic_info['fb']==""){
                                        ?>
                                        <input id="pro_fb" type="text" class="form-control" placeholder="Facebook URL">
                                        <?php
                                    } else {
                                        ?>
                                        <input id="pro_fb" value="<?php echo $basic_info['fb']; ?>" type="text" class="form-control">
                                        <?php
                                    }
                                    ?>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="pro_tw">Twitter</label>
                                    <?php
                                    if($basic_info['tw']==""){
                                        ?>
                                        <input id="pro_tw" type="text" class="form-control" placeholder="Twitter URL">
                                        <?php
                                    } else {
                                        ?>
                                        <input id="pro_tw" value="<?php echo $basic_info['tw']; ?>" type="text" class="form-control">
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="pro_gp">Google Plus</label>
                                    <?php
                                    if($basic_info['gplus']==""){
                                        ?>
                                        <input id="pro_gp" type="text" class="form-control" placeholder="GPlus URL">
                                        <?php
                                    } else {
                                        ?>
                                        <input id="pro_gp" value="<?php echo $basic_info['gplus']; ?>" type="text" class="form-control">
                                        <?php
                                    }
                                    ?>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="pro_in">LinkedIn</label>
                                    <?php
                                    if($basic_info['in']==""){
                                        ?>
                                        <input id="pro_in" type="text" class="form-control" placeholder="LinkedIn URL">
                                        <?php
                                    } else {
                                        ?>
                                        <input id="pro_in" value="<?php echo $basic_info['in']; ?>" type="text" class="form-control">
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>

                            <button type="button" id="upbaseinfo" class="btn btn-primary">Update</button>
                            <div class="alert alert-success" id="basic_msg" style="display: none;" role="alert"></div>
                            <div class="alert alert-danger" id="basic_msgerr" style="display: none;" role="alert"></div>
                        </form>
                    </div>
                    <?php endforeach; ?>
                </div>
                <!-- /.col-lg-9 -->
        </div>

    </div>
</section>
<!-- /.container -->