<section class="consectifon">
    <div class="container">

        <div class="row">
            <div class="col-lg-3">
                <?php foreach ($basic_info as $basic_pic): ?>
                    <div class="proimg">
                        <?php
                        if($basic_pic['propic']==""){
                            ?>
                            <img src="<?=base_url()?>uploads/profile-image/profile_default.jpg" alt="Card image cap">
                            <?php
                        } else {
                            ?>
                            <img src="<?php echo $basic_pic['propic']; ?>" alt="Card image cap">
                            <?php
                        }
                        ?>
                    </div>
                <?php endforeach; ?>
                <div class="list-group">
                    <a href="<?=base_url()?>profile" class="list-group-item">Profile</a>
                    <a href="<?=base_url()?>edit-profile" class="list-group-item">Edit Profile</a>
                    <a href="<?=base_url()?>update-password" class="list-group-item">Change Password</a>
                    <a href="<?=base_url()?>published-events" class="list-group-item">Published Events</a>
                    <a href="<?=base_url()?>under-review-events" class="list-group-item active">Under Review Events</a>
                    <a href="<?=base_url()?>rejected-events" class="list-group-item">Rejected Events</a>
                </div>
            </div>
            <!-- /.col-lg-3 -->

            <div class="col-lg-9">
                <div class="profilesec">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th scope="col">Event ID</th>
                            <th scope="col">Event</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if(empty($my_ureveve))
                        {
                            ?>
                            <tr>
                                <td colspan="2" class="text-center">
                                    <h5>No Events.. <a href="<?php echo base_url("create-event"); ?>">Create Events</a></h5>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                        <?php foreach ($my_ureveve as $my_urevevents): ?>
                            <tr>
                                <td>NS<?php echo $my_urevevents['eve_id']; ?></td>
                                <td><?php echo $my_urevevents['eve_title']; ?></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.col-lg-9 -->
        </div>

    </div>
</section>
<!-- /.container -->