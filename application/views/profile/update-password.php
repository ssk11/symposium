<section class="consectifon">
    <div class="container">

        <div class="row">
            <div class="col-lg-3">
                <?php foreach ($basic_info as $basic_pic): ?>
                    <div class="proimg">
                        <?php
                        if($basic_pic['propic']==""){
                            ?>
                            <img src="<?=base_url()?>uploads/profile-image/profile_default.jpg" alt="Card image cap">
                            <?php
                        } else {
                            ?>
                            <img src="<?php echo $basic_pic['propic']; ?>" alt="Card image cap">
                            <?php
                        }
                        ?>
                    </div>
                <?php endforeach; ?>
                <div class="list-group">
                    <a href="<?=base_url()?>profile" class="list-group-item">Profile</a>
                    <a href="<?=base_url()?>edit-profile" class="list-group-item">Edit Profile</a>
                    <a href="<?=base_url()?>update-password" class="list-group-item active">Change Password</a>
                    <a href="<?=base_url()?>published-events" class="list-group-item">Published Events</a>
                    <a href="<?=base_url()?>under-review-events" class="list-group-item">Under Review Events</a>
                    <a href="<?=base_url()?>rejected-events" class="list-group-item">Rejected Events</a>
                </div>
            </div>
            <!-- /.col-lg-3 -->

            <div class="col-lg-9">
                <div class="profilesec">
                    <?php
                    $old_password = array(
                        'name'	=> 'old_password',
                        'id'	=> 'old_password',
                        'class'	=> 'form-control',
                        'value' => set_value('old_password'),
                        'size' 	=> 30,
                    );
                    $new_password = array(
                        'name'	=> 'new_password',
                        'id'	=> 'new_password',
                        'class'	=> 'form-control',
                        'maxlength'	=> $this->config->item('password_max_length', 'tank_auth'),
                        'size'	=> 30,
                    );
                    $confirm_new_password = array(
                        'name'	=> 'confirm_new_password',
                        'id'	=> 'confirm_new_password',
                        'class'	=> 'form-control',
                        'maxlength'	=> $this->config->item('password_max_length', 'tank_auth'),
                        'size' 	=> 30,
                    );
                    ?>
                    <h4><?php echo $this->session->flashdata('message'); ?></h4>
                    <?php echo form_open($this->uri->uri_string()); ?>
                    <div class="form-group">
                        <?php echo form_label('Old Password', $old_password['id']); ?>
                        <?php echo form_password($old_password); ?>
                        <span style="color: red;"><?php echo form_error($old_password['name']); ?><?php echo isset($errors[$old_password['name']])?$errors[$old_password['name']]:''; ?></span>
                    </div>
                    <div class="form-group">
                        <?php echo form_label('New Password', $new_password['id']); ?>
                        <?php echo form_password($new_password); ?>
                        <span style="color: red;"><?php echo form_error($new_password['name']); ?><?php echo isset($errors[$new_password['name']])?$errors[$new_password['name']]:''; ?></span>
                    </div>
                    <div class="form-group">
                        <?php echo form_label('Confirm New Password', $confirm_new_password['id']); ?>
                        <?php echo form_password($confirm_new_password); ?>
                        <span style="color: red;"><?php echo form_error($confirm_new_password['name']); ?><?php echo isset($errors[$confirm_new_password['name']])?$errors[$confirm_new_password['name']]:''; ?></span>
                    </div>
                    <?php echo form_submit('change', 'Change Password',"class='btn btn-primary'"); ?>
                    <?php echo form_close(); ?>
                </div>
            </div>
            <!-- /.col-lg-9 -->
        </div>

    </div>
</section>
<!-- /.container -->