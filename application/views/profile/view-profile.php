<section class="consectifon">
    <div class="container">

        <div class="row">
            <?php foreach ($profile_details as $profile_details): ?>
            <div class="col-lg-3">
                <div class="proimg">
                    <?php
                    if($profile_details['propic']==""){
                        ?>
                        <img src="<?=base_url()?>uploads/profile-image/profile_default.jpg" alt="Card image cap">
                        <?php
                    } else {
                        ?>
                        <img src="<?php echo $profile_details['propic']; ?>" alt="Card image cap">
                        <?php
                    }
                    ?>
                </div>
                <div class="list-group">
                    <a href="<?=base_url()?>profile" class="list-group-item active">Profile</a>
                    <a href="<?=base_url()?>edit-profile" class="list-group-item">Edit Profile</a>
                    <a href="<?=base_url()?>update-password" class="list-group-item">Change Password</a>
                    <a href="<?=base_url()?>published-events" class="list-group-item">Published Events</a>
                    <a href="<?=base_url()?>under-review-events" class="list-group-item">Under Review Events</a>
                    <a href="<?=base_url()?>rejected-events" class="list-group-item">Rejected Events</a>
                    <?php if($this->tank_auth->get_user_id() === '1'){ ?>
                    <a href="<?=base_url("ns-admin")?>" class="list-group-item">Admin Events</a>
                    <a href="<?=base_url("ns-users")?>" class="list-group-item">Admin Users</a>
                    <?php } ?>
                </div>
            </div>
            <!-- /.col-lg-3 -->

            <div class="col-lg-9">
                <div class="profilesec">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title">FULL NAME</h5>
                                    <?php
                                    if($profile_details['fullname']==""){
                                        ?>
                                        <p class="card-text">Enter your name</p>
                                        <?php
                                    } else {
                                        ?>
                                        <p class="card-text"><?php echo $profile_details['fullname']; ?></p>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title">USERNAME</h5>
                                    <p class="card-text"><?php echo $profile_details['username']; ?> (ID: <?php echo "nsympo1".$profile_details['id']; ?>)</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title">PHONE NUMBER</h5>
                                    <?php
                                    if($profile_details['mobile']==""){
                                        ?>
                                        <p class="card-text">Enter Your Mobile Number</p>
                                        <?php
                                    } else {
                                        ?>
                                        <p class="card-text"><?php echo $profile_details['mobile']; ?></p>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title">EMAIL</h5>
                                    <p class="card-text"><?php echo $profile_details['email']; ?></p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title">GENDER</h5>
                                    <?php
                                    if($profile_details['gender']==""){
                                        ?>
                                        <p class="card-text">What's Your Gender</p>
                                        <?php
                                    } else {
                                        ?>
                                        <p class="card-text"><?php echo $profile_details['gender']; ?></p>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title">COLLEGE</h5>
                                    <?php
                                    if($profile_details['college']==""){
                                        ?>
                                        <p class="card-text">Enter Your College Name</p>
                                        <?php
                                    } else {
                                        ?>
                                        <p class="card-text"><?php echo $profile_details['college']; ?></p>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>

<!--                    <div class="row">-->
<!--                        <div class="col-sm-6">-->
<!--                            <div class="card">-->
<!--                                <div class="card-body">-->
<!--                                    <h5 class="card-title">CITY</h5>-->
<!--                                    --><?php
//                                    if($profile_details['city_name']==""){
//                                        ?>
<!--                                        <p class="card-text">Enter Your City</p>-->
<!--                                        --><?php
//                                    } else {
//                                        ?>
<!--                                        <p class="card-text">--><?php //echo $profile_details['city_name']; ?><!--</p>-->
<!--                                        --><?php
//                                    }
//                                    ?>
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="col-sm-6">-->
<!--                            <div class="card">-->
<!--                                <div class="card-body">-->
<!--                                    <h5 class="card-title">STATE</h5>-->
<!--                                    --><?php
//                                    if($profile_details['state_name']==""){
//                                        ?>
<!--                                        <p class="card-text">Enter Your State</p>-->
<!--                                        --><?php
//                                    } else {
//                                        ?>
<!--                                        <p class="card-text">--><?php //echo $profile_details['state_name']; ?><!--</p>-->
<!--                                        --><?php
//                                    }
//                                    ?>
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title">FACEBOOK</h5>
                                    <?php
                                    if($profile_details['fb']==""){
                                        ?>
                                        <p class="card-text"><a href="#">Enter Your FB id</a></p>
                                        <?php
                                    } else {
                                        ?>
                                        <p class="card-text"><a target="_blank" href=""><?php echo $profile_details['fb']; ?></a></p>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title">TWITTER</h5>
                                    <?php
                                    if($profile_details['tw']==""){
                                        ?>
                                        <p class="card-text"><a href="#">Enter Your Twitter id</a></p>
                                        <?php
                                    } else {
                                        ?>
                                        <p class="card-text"><a target="_blank" href=""><?php echo $profile_details['tw']; ?></a></p>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title">GOOGLE PLUS</h5>
                                    <?php
                                    if($profile_details['gplus']==""){
                                        ?>
                                        <p class="card-text"><a href="#">Enter Your G+ id</a></p>
                                        <?php
                                    } else {
                                        ?>
                                        <p class="card-text"><a target="_blank" href=""><?php echo $profile_details['gplus']; ?></a></p>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title">LINKEDIN</h5>
                                    <?php
                                    if($profile_details['in']==""){
                                        ?>
                                        <p class="card-text"><a href="#">Enter Your LinkedIn id</a></p>
                                        <?php
                                    } else {
                                        ?>
                                        <p class="card-text"><a target="_blank" href=""><?php echo $profile_details['in']; ?></a></p>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /.col-lg-9 -->
            <?php endforeach; ?>
        </div>

    </div>
</section>
<!-- /.container -->