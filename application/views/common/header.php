<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="description" content="<?php echo $description; ?>">
    <meta name="keywords" content="<?=$keywords;?>">
    <meta name="author" content="nationalsymposium.in">
    <meta name="robots" content="index, follow">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?=$title;?></title>
    <link rel="shortcut icon" href="<?=base_url();?>assets/images/fav.jpg"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

    <!-- Bootstrap core CSS -->
    <link href="<?=base_url();?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=base_url();?>assets/css/datepicker.css" rel="stylesheet">
    <link href="<?=base_url();?>assets/css/select2.min.css" rel="stylesheet">
    <link href="<?=base_url();?>assets/css/select2-bootstrap.css" rel="stylesheet">
    <link href="<?=base_url();?>assets/css/datepicker.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<?=base_url();?>assets/css/ns.css" rel="stylesheet">

    <script type="text/javascript" src="<?=base_url();?>assets/js/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="<?=base_url("tinymce/js/tinymce/tinymce.min.js");?>"></script>


</head>

<body>
<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="<?=base_url();?>"><img src="<?=base_url();?>assets/images/logo.png" width="190px" height="auto"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="<?=base_url('latest-events'); ?>">Latest Event</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Event Type</a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="<?=base_url("event-types/10/symposium")?>">Symposium</a>
                        <a class="dropdown-item" href="<?=base_url("event-types/2/cultural")?>">Cultural</a>
                        <a class="dropdown-item" href="<?=base_url("event-types/4/sports")?>">Sports</a>
                        <a class="dropdown-item" href="<?=base_url("event-types/9/workshop")?>">Workshop</a>
                        <a class="dropdown-item" href="<?=base_url("event-types/8/seminar")?>">Seminar</a>
                        <a class="dropdown-item" href="<?=base_url("event-types/7/conferences")?>">Conferences</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="<?=base_url("event-types")?>">More</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Search by</a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="<?=base_url("college-events-by-city")?>">City</a>
                        <a class="dropdown-item" href="<?=base_url("college-events-by-state")?>">State</a>
                        <a class="dropdown-item" href="<?=base_url("event-departments")?>">Department</a>
                        <a class="dropdown-item" href="<?=base_url("event-types")?>">Type</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link btn btn-outline-success cenavbtn" href="<?=base_url()?>create-event"><i class="fas fa-plus"></i> Create Event</a>
                </li>
                <?php
                    if(empty($_SESSION['user_id'])) {
                ?>
                <li class="nav-item">
                    <a class="nav-link" href="<?=base_url()?>login">Login</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?=base_url()?>register">Signup</a>
                </li>
                <?php
                    } else {
                ?>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Hi <?=$this->tank_auth->get_username() ?></a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="<?=base_url()?>profile">Profile</a>
                        <a class="dropdown-item" href="<?=base_url()?>edit-profile">Edit Profile</a>
                        <a class="dropdown-item" href="<?=base_url()?>published-events">My Events</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="<?=base_url()?>logout">Logout</a>
                    </div>
                </li>

                <?php
                    }
                ?>
            </ul>
        </div>
    </div>
</nav>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="gsensevertical">
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <!-- NS_topads -->
                <ins class="adsbygoogle"
                     style="display:inline-block;width:728px;height:90px"
                     data-ad-client="ca-pub-3342812138107938"
                     data-ad-slot="2333401257"></ins>
                <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                </script>
            </div>
        </div>
    </div>
</div>