<!--<div class="container">-->
<!--    <div class="row">-->
<!--        <div class="col-md-12">-->
<!--            <div class="gsensevertical">-->
<!--                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>-->
<!-- NS_bottomad -->
<!--<ins class="adsbygoogle"-->
<!--     style="display:inline-block;width:728px;height:90px"-->
<!--     data-ad-client="ca-pub-3342812138107938"-->
<!--     data-ad-slot="4947122153"></ins>-->
<!--<script>-->
<!--(adsbygoogle = window.adsbygoogle || []).push({});-->
<!--</script>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<!-- Footer -->
<section class="subscribeholder">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="subscribeholder-in">
                    <div class="row">
                        <div class="col-md-6 offset-md-3">
                            <h3>SUBSCRIBE TO GET EVENT UPDATES</h3>
                            <form action="https://feedburner.google.com/fb/a/mailverify" method="post" target="popupwindow" onsubmit="window.open('https://feedburner.google.com/fb/a/mailverify?uri=nationalsymposium/DKDa', 'popupwindow', 'scrollbars=yes,width=550,height=520');return true">
                                <div class="form-group">
                                    <input type="email" name="email" class="form-control" placeholder="Enter email">
                                    <input type="hidden" value="nationalsymposium/DKDa" name="uri"/>
                                    <input type="hidden" name="loc" value="en_US"/>
                                </div>
                                <div class="form-group text-center">
                                    <input type="submit" value="Subscribe now" class="btn btn-primary">
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="foot">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <h4><span>About</span> Us</h4>
                <p>Nationalsymposium.in is a <a href="http://www.nationalsymposium.in/">free online portal to share and find all types of college events in India.</a></p>

                    <p>The main goal of this portal is to spread all types of college events (Symposium, Workshop, Seminar, Conferences, Sports etc.,) to all college students in India.</p>
            </div>
            <div class="col-md-4">
                <h4><span>Events</span> by</h4>
                <ul class="list-group">
                    <li class="list-group-item">
                        <a href="<?=base_url("event-types")?>">Event Type</a>
                    </li>
                    <li class="list-group-item">
                        <a href="<?=base_url("event-departments")?>">Department</a>
                    </li>
                    <li class="list-group-item">
                        <a href="<?=base_url("college-events-by-city")?>">City</a>
                    </li>
                    <li class="list-group-item">
                        <a href="<?=base_url("college-events-by-state")?>">State</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-4">
                <h4><span>Useful</span> Links</h4>
                <ul class="list-group">
                    <li class="list-group-item">
                        <a href="<?=base_url("about-us")?>">About Us</a>
                    </li>
                    <li class="list-group-item">
                        <a href="<?=base_url("contact")?>">Contact Us</a>
                    </li>
                    <li class="list-group-item">
                        <a href="<?=base_url("privacy-policy")?>">Privacy Policy</a>
                    </li>
                    <li class="list-group-item">
                        <a href="#">Terms of use</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<footer>
    <div class="container">
        <p class="m-0 text-center">Copyright &copy; 2012 - 2018 National Symposium</p>
    </div>
</footer>
<!-- /.container -->

<!-- Bootstrap core JavaScript -->
<script src="<?=base_url();?>assets/vendor/jquery/jquery.min.js"></script>
<script src="<?=base_url();?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?=base_url();?>assets/vendor/bootstrap/js/bootstrap-datepicker.js"></script>
<script src="<?=base_url();?>assets/js/select2.full.js"></script>
<script src="<?=base_url();?>assets/js/typeahead.js"></script>

<script>

    var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

    var sdate = $('#ce_sdate').datepicker({
        format: 'yyyy-mm-dd',
        onRender: function(date) {
            return date.valueOf() < now.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
        if (ev.date.valueOf() > edate.date.valueOf()) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate());
            edate.setValue(newDate);
        }
        sdate.hide();
        $('#ce_edate')[0].focus();
    }).data('datepicker');
    var edate = $('#ce_edate').datepicker({
        format: 'yyyy-mm-dd',
        onRender: function(date) {
            return date.valueOf() <= sdate.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
        edate.hide();
    }).data('datepicker');
    var ce_ltdate = $('#ce_ltdate').datepicker({
        format: 'yyyy-mm-dd',
        onRender: function(date) {
            return date.valueOf() < now.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
        ce_ltdate.hide();
    }).data('datepicker');
//eyecon.ro/bootstrap-datepicker/


</script>

<script type="text/javascript">
    // Ajax post
    $(document).ready(function() {
//        $( "#ce_city" ).select2({
//            theme: "bootstrap"
//        });
        $( "#pro_city" ).select2({
            theme: "bootstrap"
        });

        // validate signup form on keyup and submit
        //$("#ce_submit").on("click",function() {
        $("#eveFm").submit(function(e) {
            e.preventDefault();
                var ce_title = $("#ce_title").val();
                var ce_sdate = $("#ce_sdate").val();
                var ce_edate = $("#ce_edate").val();
                var ce_ltdate = $("#ce_ltdate").val();
                var ce_rfee = $("#ce_rfee").val();
                var ce_decp = tinyMCE.get('ce_decp').getContent();
                var ce_evelist = tinyMCE.get('ce_evelist').getContent();
                var ce_evetypeay = [];
                $("input.evetype:checked").each(function() {
                    ce_evetypeay.push($(this).val());
                });
                var ce_evetype = ce_evetypeay.toString();
                var ce_clg = $("#ce_clg").val();
                var ce_adrs = $("#ce_adrs").val();
                var ce_city = $("#ce_city").val();
                var ce_state = $("#ce_state").val();
                var ce_pin = $("#ce_pin").val();
                var ce_evedeptay = [];
                $("input.evedept:checked").each(function() {
                    ce_evedeptay.push($(this).val());
                });
                var ce_evedept = ce_evedeptay.toString();
                var ce_banner = $("#cebanner").val();
                var ce_evweb = $("#ce_evweb").val();
                var ce_reglink = $("#ce_reglink").val();
                var ce_clglink = $("#ce_clglink").val();
                var ce_fb = $("#ce_fb").val();
                var ce_tw = $("#ce_tw").val();
                var ce_yt = $("#ce_yt").val();
                var ce_ctperson = $("#ce_ctperson").val();
                var ce_cphone = $("#ce_cphone").val();
                var ce_cmail = $("#ce_cmail").val();

                jQuery.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>" + "create_event/create",
                    dataType: 'json',
                    data: {'ce_title': ce_title, 'ce_sdate': ce_sdate, 'ce_edate': ce_edate, 'ce_ltdate': ce_ltdate, 'ce_rfee': ce_rfee, 'ce_decp': ce_decp, 'ce_evelist': ce_evelist, 'ce_evetype': ce_evetype, 'ce_clg': ce_clg, 'ce_adrs': ce_adrs, 'ce_city': ce_city, 'ce_state': ce_state, 'ce_pin': ce_pin, 'ce_evedept': ce_evedept, 'ce_banner': ce_banner, 'ce_evweb': ce_evweb, 'ce_reglink': ce_reglink, 'ce_clglink': ce_clglink, 'ce_fb': ce_fb, 'ce_tw': ce_tw, 'ce_yt': ce_yt, 'ce_ctperson': ce_ctperson, 'ce_cphone': ce_cphone, 'ce_cmail': ce_cmail},
                    success: function(success) {
                        if (success) {
                            if (success) {
                                $('#eve_error').html("<div class='error_msg'>"+success+"</div>");
                                if(success[0] == "Successfully Created" && success[1] != "some problem") {
                                    $('#ce_submit').hide();
                                    $('.ce_subpro').show().html("<i class='fas fa-sync-alt fa-spin'></i> Submitting your event.. Please wait..");
                                    $('.ce_suberr').hide();
                                    window.location.href = "<?=base_url();?>"+success[1]+"/successfully-created";

                                } else if(success[1] == "some problem") {
                                    $('.ce_suberr').show().html('Some problem occurred.');
                                    $('.ce_subpro').hide();
                                } else {
                                    $('.ce_suberr').show().html(success);
                                    $('.ce_subpro').hide();
                                }
                            }
                        }
                    },
                    beforeSend: function () {
                        $('.ce_suberr').hide();
                        $('#ce_submit').css('pointer-events','none');
                        $('.ce_subpro').show().html("<i class='fas fa-sync-alt fa-spin'></i> Submitting your event.. Please wait..");
                    },
                    complete: function () {
                        $('#ce_submit').css('pointer-events','auto');
                    }
                });

        });

        $('#ce_clg').typeahead({
            source: function(query, result) {
                $.ajax({
                    url: "<?php echo base_url(); ?>" + "create_event/getclgname",
                    method: "POST",
                    data: {query: query},
                    dataType: "json",
                    success: function (data) {
                        result($.map(data, function (item) {
                            return item.clg_name;
                        }));
                    }
                })
            }
//            minLength: 1,
//            updater: function(item) {
//                // This may need some tweaks as it has not been tested
//                var obj = JSON.parse(item.clg_adrs);
//                //return item;
//                console.log(item.clg_adrs);
//            }
//            afterSelect: function (data) {
//                //$.each( data, function( key, val ) {
//
//                    console.log(data);
//                //});
            //}
        });


        function readURL(input) {

            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#ce_banner_image').show().attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
        //upload cover image
        $('#ce_banner').change(function() {
            readURL(this);
            //alert("sds");
            var file_data = $('#ce_banner').prop('files')[0];
            var form_data = new FormData();
            form_data.append('ce_banner', file_data);
            jQuery.ajax({
                url: "<?php echo base_url(); ?>" + "create_event/upload_cover_pic", // point to server-side controller method
                dataType: 'text', // what to expect back from the server
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function (response) {
                    var getresult = response;
                    var getrebsresult = getresult.replace(/\\/g, '');
                    var getfresult = getrebsresult.replace(/\"/g, '');

                    var ckurl = new RegExp('http:');

                    if (ckurl.test(getresult) == true) {
                        $('.invalid-feedback').hide();
                        $('#cebanner').val(getfresult);
                    } else {
                        $('.invalid-feedback').show().html(response);
                    }
                },
                error: function (response) {
                    $('.invalid-feedback').html(response); // display error response from the server
                }
            });
        });

        //Select city
        $("#ce_state").on('change', function() {
            $('#ce_city').val('').removeAttr("disabled");
        });


        //selectcity
        $('#ce_city').typeahead({
            source: function(query, result)
            {
                $.ajax({
                    url:"<?php echo base_url(); ?>" + "create_event/selcity",
                    method:"POST",
                    data:{query:query,stateid: $("#ce_state").val()},
                    dataType:"json",
                    success:function(data)
                    {
                        result($.map(data, function(items){
                            return items.city_name;
                        }));
                    }
                })
            }
        });

        //Select profile city
        $("#pro_state").on('change', function() {
            var stateid = $("#pro_state").val();

            jQuery.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>" + "edit_profile/selcity",
                dataType: 'json',
                data: {'stateid': stateid},
                success: function(success) {
                    //alert(success);
                    //$('#ce_city').remove();
                    $('#pro_city').empty();

                    $.each( success, function( key, val ) {
                        $('#pro_city')
                            .append($("<option></option>")
                                .attr("value",val.city_id)
                                .text(val.city_name));
                        console.log(key+" ---- "+ val.city_name);
                    });
                },
                beforeSend: function(){
                },
                complete: function(){
                }

            });
        });

        //basic info
        $("#upbaseinfo").on("click",function() {
            var pro_name = $("#pro_name").val();
            var pro_mobile = $("#pro_mobile").val();
            var pro_gender = $("#pro_gender").val();
            var pro_clg = $("#pro_clg").val();
            var pro_city = $("#pro_city").val();
            var pro_state = $("#pro_state").val();
            var pro_fb = $("#pro_fb").val();
            var pro_tw = $("#pro_tw").val();
            var pro_gp = $("#pro_gp").val();
            var pro_in = $("#pro_in").val();
            jQuery.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>" + "edit_profile/update_basic_info",
                dataType: 'json',
                data: {'pro_name': pro_name, 'pro_mobile': pro_mobile, 'pro_gender': pro_gender, 'pro_clg': pro_clg, 'pro_city': pro_city, 'pro_state': pro_state, 'pro_fb': pro_fb, 'pro_tw': pro_tw, 'pro_gp': pro_gp, 'pro_in': pro_in},
                success: function(success) {
                    if(success == "1") {
                        $('#upbaseinfo').hide();
                        $('#basic_msg').show().html("<i class='fas fa-sync-alt fa-spin'></i> Successfully Updated...");
                        $('#basic_msgerr').hide();
                        setTimeout(function(){
                            window.location.href = "<?=base_url();?>profile";
                        }, 1);
                    }
                    if(success == "0") {
                        $('#basic_msgerr').show().html(success);
                        $('#basic_msg').hide();
                    }
                },
                beforeSend: function(){
                    $('#basic_msgerr').hide();
                    $('#upbaseinfo').css('pointer-events','none');
                    $('#basic_msg').show().html("<i class='fas fa-sync-alt fa-spin'></i> Updating your profile.. Please wait..");
                },
                complete: function(){
                    $('#upbaseinfo').css('pointer-events','auto');
                }

            });
        });

        //upload profile pic
        $('#ep_pic_btn').on('click',function() {
            var file_data = $('#ep_pic').prop('files')[0];
            var form_data = new FormData();
            form_data.append('ep_pic', file_data);
            jQuery.ajax({
                url: "<?php echo base_url(); ?>" + "edit_profile/update_propic", // point to server-side controller method
                dataType: 'text', // what to expect back from the server
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function (response) {
                    $('.cimgmsg').html("<div class='error_msg'>"+response+"</div>"); // display success response from the server
                    if(response == "1") {
                        $('.propicmsg').html("<div class='success_msg'>Image uploaded successfully. Please wait...</div>");
                        setTimeout(function(){
                            window.location.reload(1);
                        }, 1);
                    }
                    if(response == "0") {
                        $('.propicmsg').html("<div class='error_msg'>Error Occured. Try again!</div>");
                    }
                    //location.reload();
                },
                error: function (response) {
                    $('#msg').html(response); // display error response from the server
                }
            });
        });

        //updateeppo
        $("#epprove").on("click",function() {
            var eid = $("#eid").val();
            var eslug = $("#eslug").val();
            jQuery.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>" + "admin_area/eappo",
                dataType: 'json',
                data: {'eid':eid,'eslug':eslug},
                success: function(success) {
                    if(success == "1") {
                        $('#app_status').html("<div class='error_msg'>Approved Successfully!</div>");
                        setTimeout(function(){
                            window.location.href = "<?=base_url();?>ns-admin";
                        }, 1);
                    }
                    if(success == "0") {
                        $('#app_status').html("<div class='error_msg'>Error Occured. Try again!</div>");
                    }
                },
                beforeSend: function(){
                    $('#app_status').html('Approving...');
                },
                complete: function(){
                }

            });
        });

        //updaterejo
        $("#erejo").on("click",function() {
            var eid = $("#eid").val();
            var eslug = $("#eslug").val();
            jQuery.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>" + "admin_area/rejo",
                dataType: 'json',
                data: {'eid':eid,'eslug':eslug},
                success: function(success) {
                    if(success == "1") {
                        $('#app_rstatus').html("<div class='error_msg'>Rejected Successfully!</div>");
                        setTimeout(function(){
                            window.location.href = "<?=base_url();?>ns-admin";
                        }, 1);
                    }
                    if(success == "0") {
                        $('#app_rstatus').html("<div class='error_msg'>Error Occured. Try again!</div>");
                    }
                },
                beforeSend: function(){
                    $('#app_rstatus').html('Rejecting...');
                },
                complete: function(){
                }

            });
        });

        //cmail
        $("#sendcmail").on("click",function() {
            var cfullname = $("#cfullname").val();
            var cemail = $("#cemail").val();
            var cregarding = $("#cregarding").val();
            var cmsg = $("#cmsg").val();
            jQuery.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>" + "static_p/sendmail",
                dataType: 'json',
                data: {'cfullname':cfullname,'cemail':cemail,'cregarding':cregarding,'cmsg':cmsg},
                success: function(success) {
                    $('#cfail').show().html(success);
                    $('#csuccess').hide();
                    if(success == "1") {
                        $('#csuccess').show().html("Your message sent successfully!");
                        $('#cfail').hide();
                    }
                    if(success == "0") {
                        $('#cfail').show().html("Error Occured. Try again!");
                        $('#csuccess').hide();
                    }
                },
                beforeSend: function(){
                    $('#csuccess').show().html('Sending...');
                },
                complete: function(){
                }

            });
        });


//common end
    });
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-41038242-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-41038242-1');
</script>

</body>
</html>