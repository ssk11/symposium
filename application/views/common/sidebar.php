<!-- Side Widget -->
<div class="card my-4">
    <div class="card-body">
        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- NS_sidebar -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-3342812138107938"
     data-ad-slot="4492660736"
     data-ad-format="auto"
     data-full-width-responsive="true"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
    </div>
</div>

<!-- Categories Widget -->
<div class="card my-4">
    <h5 class="card-header">Events by type</h5>
    <div class="card-body">
        <div class="row">
            <div class="col-lg-6">
                <ul class="list-unstyled mb-0">
                    <li>
                        <a href="<?=base_url("event-types/10/symposium")?>">Symposium</a>
                    </li>
                    <li>
                        <a href="<?=base_url("event-types/2/cultural")?>">Cultural</a>
                    </li>
                    <li>
                        <a href="<?=base_url("event-types/4/sports")?>">Sports</a>
                    </li>
                </ul>
            </div>
            <div class="col-lg-6">
                <ul class="list-unstyled mb-0">
                    <li>
                        <a href="<?=base_url("event-types/9/workshop")?>">Workshop</a>
                    </li>
                    <li>
                        <a href="<?=base_url("event-types/8/seminar")?>">Seminar</a>
                    </li>
                    <li>
                        <a href="<?=base_url("event-types")?>">More...</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>