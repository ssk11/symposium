<section class="edit-event-holder">
    <div class="container">
        <div class="row">
            <div class="col s12 m6 l12">
                <h3 class="common-title">Edit Eventname</h3>
                <div class="editev-tab">
                    <ul class="tabs">
                        <li class="tab col s4"><a class="active" href="#edit_b">Basic Info</a></li>
                        <li class="tab col s4"><a href="#edit_t">Ticket Info</a></li>
                        <li class="tab col s4"><a href="#edit_i">Images</a></li>
                    </ul>
                </div>
                <div id="edit_b" class="col s12 edit-info-box">
                    <div class="edit-info-in">
                        <?php foreach ($eve_tks as $eve_tks): ?>
                        <div class="row">
                            <div class="col l8">
                                <h4><?php echo $eve_tks['tk_name']; ?><?php echo $eve_tks['tk_id']; ?></h4>
                            </div>
                            <div class="col l4 prc-holder">
                                Rs.<span><?php echo $eve_tks['tk_price']; ?></span> x
                                <select class="browser-default tk_count" name="etkcount[]">
                                    <option value="0">0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </select>
                                <input id="etkid" name="etkid[]" type="hidden" value="<?php echo $eve_tks['tk_id']; ?>">
                                <input id="eevid" name="eevid[]" type="hidden" value="<?php echo $eve_tks['eve_id']; ?>">
                            </div>
                        </div>
                        <?php endforeach; ?>
                        <p>Subtotal: Rs.<span id="subtotal"></span></p>
                        <p>Processing Fee: Rs.<span id="pfee"></span></p>
                        <p>Total: Rs.<span id="ftotal"></span></p>
                    </div>
                </div>
                </div>
                <div id="edit_t" class="col s12 edit-info-box">
                    <div class="edit-info-in">
                        <div class="row">
                            <form class="col s12">
                                <div class="row">
                                    <div class="input-field col s6">
                                        <select>
                                            <option value="" disabled selected>Choose Category</option>
                                            <option value="1">Option 1</option>
                                            <option value="2">Option 2</option>
                                            <option value="3">Option 3</option>
                                        </select>
                                        <label>Category</label>
                                    </div>
                                    <div class="input-field col s6">
                                        <select>
                                            <option value="" disabled selected>Choose Sub-Category</option>
                                            <option value="1">Option 1</option>
                                            <option value="2">Option 2</option>
                                            <option value="3">Option 3</option>
                                        </select>
                                        <label>Sub Category</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12 ce-evtype-holder">
                                        <p class="ce-label">Event Type</p>
                                        <p>
                                            <input name="ce_eve_type" type="radio" id="ce_isticketing" />
                                            <label for="ce_isticketing">Ticketing</label>

                                            <input name="ce_eve_type" type="radio" id="ce_isrsvp" />
                                            <label for="ce_isrsvp">RSVP</label>

                                            <input name="ce_eve_type" type="radio" id="ce_isreg" />
                                            <label for="ce_isreg">Registeration</label>
                                        </p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s3">
                                        <select>
                                            <option value="" disabled selected>Choose Ticket Type</option>
                                            <option value="1">Option 1</option>
                                            <option value="2">Option 2</option>
                                            <option value="3">Option 3</option>
                                        </select>
                                        <label>Ticket Type</label>
                                    </div>
                                    <div class="input-field col s3">
                                        <input id="ce_tkname" type="text" class="validate">
                                        <label for="ce_tkname">Ticket Name</label>
                                    </div>
                                    <div class="input-field col s2">
                                        <input id="ce_tkorder" type="text" class="validate">
                                        <label for="ce_tkorder">Order</label>
                                    </div>
                                    <div class="input-field col s2">
                                        <input id="ce_tkprice" type="text" class="validate">
                                        <label for="ce_tkprice">Price</label>
                                    </div>
                                    <div class="input-field col s1">
                                        <div class="ce-tk-icon"><i class="fa fa-cog"></i></div>
                                    </div>
                                    <div class="input-field col s1">
                                        <div class="ce-tk-icon"><i class="fa fa-trash"></i></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s6 left-align">
                                        <button class="btn waves-effect waves-light" type="submit" name="action">Add More <i class="material-icons right">queue</i></button>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="input-field col s12 right-align">
                                        <button class="btn waves-effect waves-light" type="submit" name="action">Update</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div id="edit_i" class="col s12 edit-info-box">
                    <div class="edit-info-in">
                        <div class="row">
                            <form class="col s12">
                                <div class="row">
                                    <div class="file-field input-field">
                                        <div class="btn">
                                            <span>Cover Image</span>
                                            <input type="file">
                                        </div>
                                        <div class="file-path-wrapper">
                                            <input class="file-path validate" type="text">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="file-field input-field">
                                        <div class="btn">
                                            <span>Logo</span>
                                            <input type="file">
                                        </div>
                                        <div class="file-path-wrapper">
                                            <input class="file-path validate" type="text">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="input-field col s12 right-align">
                                        <button class="btn waves-effect waves-light" type="submit" name="action">Draft</button>
                                        <button class="btn waves-effect waves-light" type="submit" name="action">Preview</button>
                                        <button class="btn waves-effect waves-light" type="submit" name="action">Publish</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div><!-- left end-->
        </div><!--rowend-->
    </div><!--container end-->
</section><!--end-->

<script>
    $("select[name='etkcount[]']").change(function() {
        //event.preventDefault();
        var etkid = $("input[name='etkid[]']").map(function(){return $(this).val();}).get();
        var eevid = $("input[name='eevid[]']").map(function(){return $(this).val();}).get();
        var etkcount = $("select[name='etkcount[]']").map(function(){return $(this).val();}).get();
        //alert(eevid);

        jQuery.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>" + "checkout_tkt/calc_tk",
            dataType: 'json',
            data: {'etkid': etkid, 'eevid': eevid, 'etkcount': etkcount},
            success: function(success) {
                //alert(success);
                $('#pfee').html(success.second);
                $('#subtotal').html(success.first);
                $('#ftotal').html(success.third);
            },
            beforeSend: function(){
                //$('.preloader-wrapper').show();
                //$('.overlay').show();
            },
            complete: function(){
                //$('.preloader-wrapper').hide();
                //$('.overlay').hide();
            }

        });
    });
</script>
