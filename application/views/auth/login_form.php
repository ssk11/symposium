<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="description" content="Nationalsymposium.in is a free online portal to share and find all types of college events in India.">
	<meta name="keywords" content="symposium,college events in India,college events,workshops">
	<meta name="author" content="Nationalsymposium">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Login - National Symposium</title>
	<link rel="shortcut icon" href="<?=base_url();?>assets/images/fav.jpg"/>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

	<!-- Bootstrap core CSS -->
	<link href="<?=base_url();?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<!-- Custom styles for this template -->
	<link href="<?=base_url();?>assets/css/ns.css" rel="stylesheet">

	<script type="text/javascript" src="<?=base_url();?>assets/js/jquery-1.8.3.min.js"></script>

</head>
<body class="authbody">
<?php
$login = array(
	'name'	=> 'login',
	'id'	=> 'login',
	'class'	=> 'form-control',
	'value' => set_value('login'),
	'maxlength'	=> 80,
	'size'	=> 30,
);
if ($login_by_username AND $login_by_email) {
	$login_label = 'Email or login';
} else if ($login_by_username) {
	$login_label = 'Login';
} else {
	$login_label = 'Email';
}
$password = array(
	'name'	=> 'password',
	'id'	=> 'password',
	'class'	=> 'form-control',
	'size'	=> 30,
);
$remember = array(
	'name'	=> 'remember',
	'id'	=> 'remember',
	'value'	=> 1,
	'checked'	=> set_value('remember'),
	'style' => 'margin:0;padding:0',
);
$captcha = array(
	'name'	=> 'captcha',
	'id'	=> 'captcha',
	'maxlength'	=> 8,
);
?>


<div class="container">
	<div class="row">
		<div class="col-md-6 offset-md-3">
			<div class="auth-logo-holder">
				<a href="<?=base_url();?>"><img src="<?=base_url()?>assets/images/logo.png" width="180px" height="auto"></a>
			</div>
			<div class="auth-holder">
				<h1>Login</h1>
				<hr>
				<?php echo form_open($this->uri->uri_string()); ?>
				<div class="form-group">
					<?php echo form_label($login_label, $login['id']); ?>
					<?php echo form_input($login); ?>
					<span style="color: red;"><?php echo form_error($login['name']); ?><?php echo isset($errors[$login['name']])?$errors[$login['name']]:''; ?></span>
				</div>
				<div class="form-group">
					<?php echo form_label('Password', $password['id']); ?>
					<?php echo form_password($password); ?>
					<span style="color: red;"><?php echo form_error($password['name']); ?><?php echo isset($errors[$password['name']])?$errors[$password['name']]:''; ?></span>
				</div>
				<div class="form-group form-check">
					<?php echo form_checkbox($remember); ?>
					<?php echo form_label('Remember me', $remember['id']); ?>
				</div>

				<div class="row">
					<div class="col">
						<?php echo anchor('forgot-password/', 'Forgot password'); ?>&nbsp;&nbsp;
						<?php if ($this->config->item('allow_registration', 'tank_auth')) echo anchor('register/', 'Register'); ?>
					</div>
					<div class="col text-right">
						<?php echo form_submit('submit', 'Let me in',"class='btn btn-primary'"); ?>
					</div>
				</div>
				<?php echo form_close(); ?>
			</div>
			<div class="auth-foot">
				<p><a href="<?=base_url();?>">Back to homepage</a></p>
			</div>
		</div>
	</div>
</div>



<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="gsensevertical" style="height: 90px;"></div>
		</div>
	</div>
</div>

<!-- Bootstrap core JavaScript -->
<script src="<?=base_url();?>assets/vendor/jquery/jquery.min.js"></script>
<script src="<?=base_url();?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-41038242-1"></script>
<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'UA-41038242-1');
</script>

</body>
</html>