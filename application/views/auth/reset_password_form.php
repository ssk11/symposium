<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="description" content="Nationalsymposium.in is a free online portal to share and find all types of college events in India.">
	<meta name="keywords" content="symposium,college events in India,college events,workshops">
	<meta name="author" content="Nationalsymposium">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Reset Password - National Symposium</title>
	<link rel="shortcut icon" href="<?=base_url();?>assets/images/fav.jpg"/>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

	<!-- Bootstrap core CSS -->
	<link href="<?=base_url();?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<!-- Custom styles for this template -->
	<link href="<?=base_url();?>assets/css/ns.css" rel="stylesheet">

	<script type="text/javascript" src="<?=base_url();?>assets/js/jquery-1.8.3.min.js"></script>

</head>
<?php
$new_password = array(
	'name'	=> 'new_password',
	'class'	=> 'form-control',
	'id'	=> 'new_password',
	'maxlength'	=> $this->config->item('password_max_length', 'tank_auth'),
	'size'	=> 30,
);
$confirm_new_password = array(
	'name'	=> 'confirm_new_password',
	'class'	=> 'form-control',
	'id'	=> 'confirm_new_password',
	'maxlength'	=> $this->config->item('password_max_length', 'tank_auth'),
	'size' 	=> 30,
);
?>

<div class="container">
	<div class="row">
		<div class="col-md-6 offset-md-3">
			<div class="auth-logo-holder">
				<a href="<?=base_url();?>"><img src="<?=base_url()?>assets/images/logo.png" width="180px" height="auto"></a>
			</div>
			<div class="auth-holder">
				<h1>Reset Password</h1>
				<hr>
				<?php echo form_open($this->uri->uri_string()); ?>
				<div class="form-group">
					<?php echo form_label('New Password', $new_password['id']); ?>
					<?php echo form_password($new_password); ?>
					<span style="color: red;"><?php echo form_error($new_password['name']); ?><?php echo isset($errors[$new_password['name']])?$errors[$new_password['name']]:''; ?></span>
				</div>
				<div class="form-group">
					<?php echo form_label('Confirm New Password', $confirm_new_password['id']); ?>
					<?php echo form_password($confirm_new_password); ?>
					<span style="color: red;"><?php echo form_error($confirm_new_password['name']); ?><?php echo isset($errors[$confirm_new_password['name']])?$errors[$confirm_new_password['name']]:''; ?></span>
				</div>
				<div class="row">
					<div class="col text-right">
						<?php echo form_submit('change', 'Change Password',"class='btn btn-primary'"); ?>
					</div>
				</div>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
</div>

<!-- Bootstrap core JavaScript -->
<script src="<?=base_url();?>assets/vendor/jquery/jquery.min.js"></script>
<script src="<?=base_url();?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-41038242-1"></script>
<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'UA-41038242-1');
</script>

</body>
</html>
