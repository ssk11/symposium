<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="description" content="Nationalsymposium.in is a free online portal to share and find all types of college events in India.">
	<meta name="keywords" content="symposium,college events in India,college events,workshops">
	<meta name="author" content="Nationalsymposium">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Send again - National Symposium</title>
	<link rel="shortcut icon" href="<?=base_url();?>assets/images/fav.jpg"/>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

	<!-- Bootstrap core CSS -->
	<link href="<?=base_url();?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<!-- Custom styles for this template -->
	<link href="<?=base_url();?>assets/css/ns.css" rel="stylesheet">

	<script type="text/javascript" src="<?=base_url();?>assets/js/jquery-1.8.3.min.js"></script>

</head>
<?php
$email = array(
	'name'	=> 'email',
	'id'	=> 'email',
	'value'	=> set_value('email'),
	'maxlength'	=> 80,
	'size'	=> 30,
);
?>

<div class="container">
	<div class="row">
		<div class="col-md-6 offset-md-3">
			<div class="auth-logo-holder">
				<a href="<?=base_url();?>"><img src="<?=base_url()?>assets/images/logo.png" width="180px" height="auto"></a>
			</div>
			<div class="auth-holder">
				<h1>Send Again</h1>
				<hr>
				<?php echo form_open($this->uri->uri_string()); ?>
				<div class="form-group">
					<?php echo form_label('Email Address', $email['id']); ?>
					<?php echo form_input($email); ?>
					<span style="color: red;"><?php echo form_error($email['name']); ?><?php echo isset($errors[$email['name']])?$errors[$email['name']]:''; ?></span>
				</div>
				<div class="row">
					<div class="col text-right">
						<?php echo form_submit('send', 'Send',"class='btn btn-primary'"); ?>
					</div>
				</div>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="gsensevertical" style="height: 90px;"></div>
		</div>
	</div>
</div>

<!-- Bootstrap core JavaScript -->
<script src="<?=base_url();?>assets/vendor/jquery/jquery.min.js"></script>
<script src="<?=base_url();?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-41038242-1"></script>
<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'UA-41038242-1');
</script>

</body>
</html>