<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="description" content="Nationalsymposium.in is a free online portal to share and find all types of college events in India.">
    <meta name="keywords" content="symposium,college events in India,college events,workshops">
    <meta name="author" content="Nationalsymposium">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>National Symposium</title>
    <link rel="shortcut icon" href="<?=base_url();?>assets/images/fav.jpg"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

    <!-- Bootstrap core CSS -->
    <link href="<?=base_url();?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<?=base_url();?>assets/css/ns.css" rel="stylesheet">

    <script type="text/javascript" src="<?=base_url();?>assets/js/jquery-1.8.3.min.js"></script>

</head>
<body class="authbody">
<div class="container">
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <div class="auth-logo-holder">
                <a href="<?=base_url();?>"><img src="<?=base_url()?>assets/images/logo.png" width="180px" height="auto"></a>
            </div>
            <div class="auth-holder">
                <h1><?php echo $message; ?></h1>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="gsensevertical">
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <!-- NS_topads -->
                <ins class="adsbygoogle"
                     style="display:inline-block;width:728px;height:90px"
                     data-ad-client="ca-pub-3342812138107938"
                     data-ad-slot="2333401257"></ins>
                <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                </script>
            </div>
        </div>
    </div>
</div>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-41038242-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-41038242-1');
</script>

</body>
</html>
