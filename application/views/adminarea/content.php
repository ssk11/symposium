<!-- Page Content -->
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <p><strong><?php echo $total_records; ?></strong> Events waiting for approval</p>
        </div>
    </div>
    <hr class="homehr">
</div>
<div class="container">

    <div class="row">

        <!-- Blog Entries Column -->
        <div class="col-md-8">

            <h1 class="my-4 evesubtitle">Events Queue</h1>

            <?php
            if(empty($latest_event)) {
                ?>
                <h6>Sorry no events found right now..</h6>
                <?php
            } else {
                ?>
                <!-- Blog Post -->
                <?php foreach ($latest_event as $latest_event): ?>
                    <div class="card mb-4">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <a href="<?php echo base_url(); ?>nse/<?php echo $latest_event['eve_id']; ?>/<?php echo $latest_event['eve_slug']; ?>"><img src="<?php echo $latest_event['eve_banner']; ?>" alt="Card image cap" width="100%" height="auto"></a>
                                </div>
                                <div class="col-md-9">
                                    <h2 class="card-title"><a href="<?php echo base_url(); ?>nse/<?php echo $latest_event['eve_id']; ?>/<?php echo $latest_event['eve_slug']; ?>"><?php echo $latest_event['eve_title']; ?></a></h2>
                                    <p class="card-text"><?php echo $latest_event['eve_clg']; ?>, <?php echo $latest_event['eve_city']; ?>,<?php echo $latest_event['state_name']; ?></p>
                                    <div class="text-muted">
                                        <?php
                                        $postdate = $latest_event['eve_postdate'];
                                        $y = date('Y',strtotime($postdate));
                                        $d = date('d',strtotime($postdate));
                                        $m = date('M',strtotime($postdate));
                                        ?>
                                        Posted on <?php echo $m; ?> <?php echo $d; ?>, <?php echo $y; ?> by
                                        <a href="#"><?php echo $latest_event['eve_puser']; ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; } ?>

            <!-- Pagination -->
            <?php if (isset($links)) { ?>
                <?php echo $links ?>
            <?php } ?>

        </div>

        <!-- Sidebar Widgets Column -->
        <div class="col-md-4">

            <!-- Search Widget -->
            <div class="card my-4">
                <h5 class="card-header">Search</h5>
                <div class="card-body">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search for...">
                <span class="input-group-btn">
                  <button class="btn btn-secondary" type="button">Go!</button>
                </span>
                    </div>
                </div>
            </div>
            <?php $this->load->view('common/sidebar'); ?>
        </div>

    </div>
    <!-- /.row -->

</div>
<!-- /.container -->