<!-- Page Content -->
<div class="container">

    <div class="row">
        <div class="col-md-6">
            <button id="epprove" type="button" class="btn btn-secondary">Approve</button>
            <div id="app_status"></div>
        </div>
        <div class="col-md-6">
            <button id="erejo" type="button" class="btn btn-secondary">Reject</button>
            <div id="app_rstatus"></div>
        </div>
    </div>
    <div class="row">
        <?php foreach ($event_details as $event_details): ?>
            <!-- Post Content Column -->
            <div class="col-lg-8">

                <!-- Title -->
                <h1 class="mt-4 evesubtitle"><?php echo $event_details['eve_title']; ?></h1>

                <!-- Author -->
                <p class="lead">
                    by
                    <a href="#"><?php echo $event_details['eve_puser']; ?></a>
                </p>

                <hr>

                <!-- Date/Time -->
                <?php
                $postdate = $event_details['eve_postdate'];
                $y = date('Y',strtotime($postdate));
                $d = date('d',strtotime($postdate));
                $m = date('M',strtotime($postdate));
                ?>
                <p>Posted on <?php echo $m; ?> <?php echo $d; ?>, <?php echo $y; ?></p>

                <hr>

                <!-- Preview Image -->
                <img class="img-fluid rounded" src="<?php echo $event_details['eve_banner']; ?>" width="100%" height="auto" alt="">

                <hr>

                <ul class="nav nav-pills eve-nav mb-3" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Description</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Event List</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">Event Venue</a>
                    </li>
                </ul>
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                        <?php echo html_entity_decode($event_details['eve_description']); ?>
                        <input id="eid" type="hidden" value="<?php echo $event_details['eve_id']; ?>">
                        <input id="eslug" type="hidden" value="<?php echo $event_details['eve_slug']; ?>">
                        <ul class="list-group">
                            <?php if($event_details['eve_evweb'] !=""){ ?>
                                <li class="list-group-item">Event Website: <b><a href="<?php echo $event_details['eve_evweb']; ?>"><?php echo $event_details['eve_evweb']; ?></a></b></li>
                            <?php } ?>
                            <?php if($event_details['eve_reglink'] !=""){ ?>
                                <li class="list-group-item">Registration Link: <b><a href="<?php echo $event_details['eve_reglink']; ?>"><?php echo $event_details['eve_reglink']; ?></a></b></li>
                            <?php } ?>
                            <?php if($event_details['eve_clglink'] !=""){ ?>
                                <li class="list-group-item">College Website: <b><a href="<?php echo $event_details['eve_clglink']; ?>"><?php echo $event_details['eve_clglink']; ?></a></b></li>
                            <?php } ?>
                            <?php if($event_details['eve_fb'] !=""){ ?>
                                <li class="list-group-item">Facebook URL: <b><a href="<?php echo $event_details['eve_fb']; ?>"><?php echo $event_details['eve_fb']; ?></a></b></li>
                            <?php } ?>
                            <?php if($event_details['eve_tw'] !=""){ ?>
                                <li class="list-group-item">Twitter URL: <b><a href="<?php echo $event_details['eve_tw']; ?>"><?php echo $event_details['eve_tw']; ?></a></b></li>
                            <?php } ?>
                            <?php if($event_details['eve_yt'] !=""){ ?>
                                <li class="list-group-item">Youtube URL: <b><a href="<?php echo $event_details['eve_yt']; ?>"><?php echo $event_details['eve_yt']; ?></a></b></li>
                            <?php } ?>
                        </ul>
                    </div>
                    <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab"><?php echo html_entity_decode($event_details['eve_evelist']); ?></div>
                    <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                        <h5><?php echo $event_details['eve_clg']; ?></h5>
                        <address><?php echo $event_details['eve_adrs']; ?><br>
                            <?php echo $event_details['eve_city']; ?><br>
                            <?php echo $event_details['state_name']; ?></address></div>
                </div>
                <ul class="list-group">
                    <?php
                    $evedate = $event_details['eve_sdate'];
                    $evey = date('Y',strtotime($evedate));
                    $eved = date('d',strtotime($evedate));
                    $evem = date('M',strtotime($evedate));

                    $lastdate = $event_details['eve_ltdate'];
                    $lasty = date('Y',strtotime($lastdate));
                    $lastd = date('d',strtotime($lastdate));
                    $lastm = date('M',strtotime($lastdate));
                    ?>
                    <li class="list-group-item">Event Date: <b><?php echo $evem; ?> <?php echo $eved; ?>, <?php echo $evey; ?></b></li>
                    <li class="list-group-item">Last Date to Register: <b><?php echo $lastm; ?> <?php echo $lastd; ?>, <?php echo $lasty; ?></b></li>
                    <li class="list-group-item">Registration Fee: <b>Rs.<?php echo $event_details['eve_rfee']; ?></b></li>
                </ul>
                <br>
                <p><b>Event Type: </b>
                    <?php
                    foreach ($event_details['type_name'] as $typeKey => $typeName){
                        ?>
                        <a class="evepagelink" href="<?=base_url("event-types/".$typeKey."/".strtolower(str_replace(" ","-",$typeName)))?>"><?=$typeName?></a>
                        <?php
                    }
                    ?>
                </p>
                <p><b>Event Department: </b>
                    <?php
                    foreach ($event_details['dept_name'] as $typeKey => $typeName){
                        ?>
                        <a class="evepagelink" href="<?=base_url("event-departments/".$typeKey."/".strtolower(str_replace(" ","-",$typeName)))?>"><?=$typeName?></a>
                        <?php
                    }
                    ?>
                </p>
                <hr>

            </div>

            <!-- Sidebar Widgets Column -->
            <div class="col-md-4">

                <!-- Search Widget -->
                <div class="card my-4">
                    <h5 class="card-header">Organizer Details</h5>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <ul class="list-unstyled mb-0">
                                    <li>
                                        <i class="fas fa-user"></i> <b><?php echo $event_details['eve_ctperson']; ?></b>
                                    </li>
                                    <li>
                                        <i class="fas fa-phone"></i> <i><?php echo $event_details['eve_cphone']; ?></i>
                                    </li>
                                    <li>
                                        <i class="fas fa-envelope"></i> <i><?php echo $event_details['eve_cmail']; ?></i>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Categories Widget -->
                <div class="card my-4">
                    <h5 class="card-header">Categories</h5>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <ul class="list-unstyled mb-0">
                                    <li>
                                        <a href="#">Web Design</a>
                                    </li>
                                    <li>
                                        <a href="#">HTML</a>
                                    </li>
                                    <li>
                                        <a href="#">Freebies</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-lg-6">
                                <ul class="list-unstyled mb-0">
                                    <li>
                                        <a href="#">JavaScript</a>
                                    </li>
                                    <li>
                                        <a href="#">CSS</a>
                                    </li>
                                    <li>
                                        <a href="#">Tutorials</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Side Widget -->
                <div class="card my-4">
                    <h5 class="card-header">Side Widget</h5>
                    <div class="card-body">
                        You can put anything you want inside of these side widgets. They are easy to use, and feature the new Bootstrap 4 card containers!
                    </div>
                </div>

            </div>

        <?php endforeach; ?>

    </div>
    <!-- /.row -->

</div>
<!-- /.container -->