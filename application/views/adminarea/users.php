<!-- Page Content -->
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <p><strong><?php echo $Usercount; ?></strong> Users</p>
        </div>
    </div>
    <hr class="homehr">
</div>
<div class="container">

    <div class="row">

        <!-- Blog Entries Column -->
        <div class="col-md-8">

            <h1 class="my-4 evesubtitle">Events Queue</h1>

            <?php
            if(empty($all_users)) {
                ?>
                <h6>No Users</h6>
                <?php
            } else {
                ?>
                <!-- Blog Post -->

                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Username</th>
                            <th scope="col">Fullname</th>
                            <th scope="col">Email</th>
                            <th scope="col">Status</th>
                        </tr>
                        </thead>
                        <tbody>
                <?php foreach ($all_users as $all_users): ?>
                        <tr>
                            <th scope="row"><?php echo $all_users['id']; ?></th>
                            <td><?php echo $all_users['username']; ?></td>
                            <td><?php echo $all_users['fullname']; ?></td>
                            <td><?php echo $all_users['email']; ?></td>
                            <td><?php echo $all_users['activated']; ?></td>
                        </tr>
                <?php endforeach; } ?>
                        </tbody>
                    </table>


        </div>

        <!-- Sidebar Widgets Column -->
        <div class="col-md-4">

            <!-- Search Widget -->
            <div class="card my-4">
                <h5 class="card-header">Search</h5>
                <div class="card-body">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search for...">
                <span class="input-group-btn">
                  <button class="btn btn-secondary" type="button">Go!</button>
                </span>
                    </div>
                </div>
            </div>
            <?php $this->load->view('common/sidebar'); ?>
        </div>

    </div>
    <!-- /.row -->

</div>
<!-- /.container -->