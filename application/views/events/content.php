<!-- Page Content -->
<div class="container">

    <div class="row">
        <?php foreach ($event_details as $event_details): ?>
        <!-- Post Content Column -->
        <div class="col-lg-8">

            <!-- Title -->
            <h1 class="mt-4 evesubtitle"><?php echo $event_details['eve_title']; ?></h1>

            <!-- Author -->
            <p class="lead">
                by
                <a href="#"><?php echo $event_details['eve_puser']; ?></a>
            </p>



            <!-- Date/Time -->

            <div class="row eviewholder">
                <div class="col-md-9">
                    <?php
                    $postdate = $event_details['eve_postdate'];
                    $y = date('Y',strtotime($postdate));
                    $d = date('d',strtotime($postdate));
                    $m = date('M',strtotime($postdate));
                    ?>
                    <p>Posted on <?php echo $m; ?> <?php echo $d; ?>, <?php echo $y; ?></p>
                </div>
                <div class="col-md-3">

                    <i class="fas fa-eye"></i> <?php echo $event_details['eve_views']; ?>
                </div>
            </div>
            <div class="row eshareholder">
                <div class="col-md-12">
                    <ul>
                        <li>Share:</li>
                        <li class="fb"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo base_url("e/".$event_details['eve_id']."/".$event_details['eve_slug']); ?>"><i class="fab fa-facebook-square"></i></a></li>
                        <li class="tw"><a target="_blank" href="http://twitter.com/share?text=<?php echo $event_details['eve_title']; ?>&url=<?php echo base_url("e/".$event_details['eve_id']."/".$event_details['eve_slug']); ?>&hashtags=nationalsymposium,collegeevents"><i class="fab fa-twitter-square"></i></a></li>
                        <li class="wup"><a href="whatsapp://send?text=<?php echo base_url("e/".$event_details['eve_id']."/".$event_details['eve_slug']); ?>" data-action="share/whatsapp/share"><i class="fab fa-whatsapp-square"></i></a></li>
                    </ul>
                </div>
            </div>
            <hr>

            <!-- Preview Image -->
            <?php
                if($event_details['eve_banner'] == ""){
            ?>
                    <img class="img-fluid rounded" src="<?php echo base_url('uploads/cover-image/default-cover-image.jpg') ?>" width="100%" height="auto" alt="">
            <?php
                }
            ?>
            <img class="img-fluid rounded" src="<?php echo $event_details['eve_banner']; ?>" width="100%" height="auto" alt="">

            <hr>

            <ul class="nav nav-pills eve-nav mb-3" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Description</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Event List</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">Event Venue</a>
                </li>
            </ul>
            <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                    <?php echo html_entity_decode($event_details['eve_description']); ?>
                    <ul class="list-group evelistg">
                        <?php if($event_details['eve_evweb'] !=""){ ?>
                            <li class="list-group-item">Event Website: <b><a class="btn btn-info" target="_blank" href="<?php echo $event_details['eve_evweb']; ?>">Go to event website</a></b></li>
                        <?php } ?>
                        <?php if($event_details['eve_reglink'] !=""){ ?>
                            <li class="list-group-item">Registration Link: <b><a class="btn btn-info" target="_blank" href="<?php echo $event_details['eve_reglink']; ?>">Register now</a></b></li>
                        <?php } ?>
                        <?php if($event_details['eve_clglink'] !=""){ ?>
                            <li class="list-group-item">College Website: <b><a class="btn btn-info" target="_blank" href="<?php echo $event_details['eve_clglink']; ?>">Go to college website</a></b></li>
                        <?php } ?>
                        <?php if($event_details['eve_fb'] !=""){ ?>
                            <li class="list-group-item">Facebook URL: <b><a target="_blank" href="<?php echo $event_details['eve_fb']; ?>"><?php echo $event_details['eve_fb']; ?></a></b></li>
                        <?php } ?>
                        <?php if($event_details['eve_tw'] !=""){ ?>
                            <li class="list-group-item">Twitter URL: <b><a target="_blank" href="<?php echo $event_details['eve_tw']; ?>"><?php echo $event_details['eve_tw']; ?></a></b></li>
                        <?php } ?>
                        <?php if($event_details['eve_yt'] !=""){ ?>
                            <li class="list-group-item">Youtube URL: <b><a target="_blank" href="<?php echo $event_details['eve_yt']; ?>"><?php echo $event_details['eve_yt']; ?></a></b></li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab"><?php echo html_entity_decode($event_details['eve_evelist']); ?></div>
                <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                    <h5><?php echo $event_details['eve_clg']; ?></h5>
                    <address><?php echo $event_details['eve_adrs']; ?><br>
                    <?php echo $event_details['eve_city']; ?><br>
                    <?php echo $event_details['state_name']; ?><br>
                    Zip Code: <?php echo $event_details['eve_pin']; ?>
                    </address></div>
            </div>
            <ul class="list-group">
                <?php
                $evedate = $event_details['eve_sdate'];
                $evey = date('Y',strtotime($evedate));
                $eved = date('d',strtotime($evedate));
                $evem = date('M',strtotime($evedate));

                $lastdate = $event_details['eve_ltdate'];
                $lasty = date('Y',strtotime($lastdate));
                $lastd = date('d',strtotime($lastdate));
                $lastm = date('M',strtotime($lastdate));
                ?>
                <li class="list-group-item">Event Date: <b><?php echo $evem; ?> <?php echo $eved; ?>, <?php echo $evey; ?></b></li>
                <li class="list-group-item">Last Date to Register: <b><?php echo $lastm; ?> <?php echo $lastd; ?>, <?php echo $lasty; ?></b></li>
                <?php if($event_details['eve_rfee'] !=""){ ?>
                <li class="list-group-item">Registration Fee: <b>Rs.<?php echo $event_details['eve_rfee']; ?></b></li>
                <?php } ?>
            </ul>
<br>
            <p><b>Event Type: </b>
                <?php
                    foreach ($event_details['type_name'] as $typeKey => $typeName){
                        ?>
                        <a class="evepagelink" href="<?=base_url("event-types/".$typeKey."/".strtolower(str_replace(" ","-",$typeName)))?>"><?=$typeName?></a>
                <?php
                    }
                ?>
            </p>
            <p><b>Event Department: </b>
                <?php
                foreach ($event_details['dept_name'] as $typeKey => $typeName){
                    ?>
                    <a class="evepagelink" href="<?=base_url("event-departments/".$typeKey."/".strtolower(str_replace(" ","-",$typeName)))?>"><?=$typeName?></a>
                    <?php
                }
                ?>
            </p>
            <hr>

        </div>

        <!-- Sidebar Widgets Column -->
        <div class="col-md-4">

            <!-- Search Widget -->
            <div class="card my-4">
                <h5 class="card-header">Organizer Details</h5>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <ul class="list-unstyled mb-0">
                                <li>
                                    <i class="fas fa-user"></i> <b><?php echo $event_details['eve_ctperson']; ?></b>
                                </li>
                                <li>
                                    <i class="fas fa-phone"></i> <i><?php echo $event_details['eve_cphone']; ?></i>
                                </li>
                                <li>
                                    <i class="fas fa-envelope"></i> <i><?php echo $event_details['eve_cmail']; ?></i>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

                <?php $this->load->view('common/sidebar'); ?>

        </div>

            <div class="callmob">
                <a href="tel:<?php echo $event_details['eve_cphone']; ?>"><div class="callmobin">Call Organizer <i class="fas fa-phone"></i></div></a>
            </div>

        <?php endforeach; ?>

    </div>
    <!-- /.row -->

</div>
<!-- /.container -->
